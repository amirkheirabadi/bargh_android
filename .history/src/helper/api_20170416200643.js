'use strict'
import React, {
    Component
} from 'react';
import Toast from 'react-native-root-toast';
import RNFetchBlob from 'react-native-fetch-blob'
import {
    AsyncStorage
} from 'react-native';
var config = require('../config.js');
class Api {
    async request(address, method, data, files = []) {
        var headers = {
            'Content-type': 'multipart/form-data'
        };
        let user = await this.userFetch();
        if (user) {
            user = JSON.parse(user);
            console.log('token', user['token']);
            headers['Authorization'] = 'Bearer ' + user['token'];
        }
        var reqData = [];
        for (var req in data) {
            var value = data[req];
            if (typeof data[req] == "object" || typeof data[req] == "array") {
                value = JSON.stringify(data[req]);
            }
            if (typeof data[req] == "number") {
                value = data[req] + '';
            }
            reqData.push({
                name: req,
                data: value
            });
        }
        for (var file in files) {
            if (files[file] != undefined) {
                if (typeof files[file] == "object") {
                    var counter = 1;
                    files[file].forEach(function(item, index) {
                        if (item != "" && item != undefined) {
                            item = item.replace("file://", "");
                            reqData.push({
                                name: file + '_' + counter,
                                filename: file + '_' + counter + '.jpg',
                                data: RNFetchBlob.wrap(item)
                            });
                            counter++;
                        }
                    })
                } else {
                    var filename = file + '.jpg';
                    var data = files[file];
                    if (files[file]['filename'] != undefined && files[file]['data'] != undefined) {
                        filename = files[file]['filename'];
                        data = files[file]['data'];
                    }
                    var property = {
                        name: file,
                        filename: filename,
                        data: RNFetchBlob.wrap(data)
                    };
                    if (files[file]['type'] != undefined) {
                        property['type'] = files[file]['type'];
                    }
                    reqData.push(property);
                }
            }
        }
        console.log(config.server + '/api/' + address);
        var request = RNFetchBlob.fetch(method,config.server + '/api/' + address, headers, reqData);
        return await request.then((resp) => {
            console.log(resp.text());
            let responseJson = resp.json();
            if (resp.respInfo.status >= 500) {
                console.log(resp.respInfo);
                return;
            }
            if (responseJson.message.length) {
                var toastMessage = "";
                var index = 1;
                console.log(responseJson.message.length);
                responseJson.message.forEach(function(message) {
                    console.log(index);
                    toastMessage = toastMessage + message;
                    if (responseJson.message.length < index) {
                        toastMessage = toastMessage + 'amir';
                    }
                    index++;
                });
              
                Toast.show(toastMessage, {
                    duration: Toast.durations.LONG,
                    position: -50,
                    backgroundColor: '#e74c3c',
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    textStyle: {
                        color: '#fff'
                    }
                });
            }
            return [resp.respInfo.status, responseJson];
        }).catch((err) => {
            console.log(err);
        });
    }

    async userProfile() {
        let user = await this.request('user/profile', 'GET', []);
        let save = await this.userStore(user[1]['data']);
        return user[1]['data'];
    }

    async userStore(user) {
        try {
            await AsyncStorage.setItem("USER_DATA", user);
            return true;
        } catch (e) {
            return false;
        }
    }

    async userFetch() {
        try {
            const user = await AsyncStorage.getItem('USER_DATA');
            return user;
        } catch (e) {
            return null;
        }
    }

    async userSignout() {
        try {
            let status = await AsyncStorage.removeItem('USER_DATA');
            return true;
        } catch (e) {
            return false;
        }
    }
}
export default new Api();
