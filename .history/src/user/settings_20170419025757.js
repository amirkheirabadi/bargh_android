import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import {Actions} from 'react-native-router-flux';
import {Text, View, Dimensions, TouchableWithoutFeedback, Modal, BackAndroid, Keyboard} from 'react-native';
import Api from '../helper/api';
var styles = require('../style.js');
export default class Settings extends Component {
    constructor(props) {
        super(props);

        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            mobile: '',
            title: 'ویرایش نام',
            inputField: '',
            inputValue: '',

            oldPassword: '',
            'newPassword': '',
            'confirmPassword': '',

            modalVisible: false
        };
    }

    async componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
                Actions.Home();
                return true;
            }
        );
        let user = await Api.userProfile();
        this.setState({
            first_name: user['first_name'],
            last_name: user['last_name'],
            email: user['email'],
            mobile: user['mobile']
        });


    }

    edit(param) {
        trans = {
            first_name: 'نام',
            last_name: 'نام خانوادگی',
            email: 'ایمیل',
            mobile: 'موبایل'
        };

        this.setState({
            modalVisible: true,
            title: 'ویرایش ' +
                    trans[param],
            inputField: param,
            inputValue: this.state[param]
        });
    }

    async confirm() {
        Keyboard.dismiss();
        let status = await Api.request('user/profile',
            'POST', {
            type: this.state.inputField,
            value: this.state.inputValue
        });
        if (status[0] == 200) {
            if (this.state.inputField == "email" || this.state.inputField == "mobile") {
                Actions.ProfileConfirm({'data' : {type: this.state.inputField}});
            }
            changes = {};
            changes[this.state.inputField] = this.state.inputValue;
            this.setState(changes);
        }

        this.setState({modalVisible: false});
    }

    async changePassword() {
        if (!this.state.oldPassword) {
            Toast.show('لطفا رمز عبور کنونی خود را وارد کنید .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#e74c3c',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle: {
                    color: '#fff',
                    fontFamily: 'IRANSans_Light'
                }
            }
            );
            return;
        }
        Keyboard.dismiss();
        if (!this.state.newPassword || !this.state.confirmPassword) {
            Toast.show('لطفا رمز عبور جدید خود به همراه تکرارش را وارد نمایید .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#e74c3c',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle: {
                    color: '#fff',
                    fontFamily: 'IRANSans_Light'
                }
            }
            );
            return;
        }
        let status = await Api.request('user/password',
            'POST', {
            old_password: this.state.oldPassword,
            password: this.state.newPassword,
            password_confirmation: this.state.confirmPassword
        }
        );

        if (status[0] == 200) {
            Toast.show('رمز عبور شما با موفقت تعویض شد .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#2ecc71',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle: {
                    color: '#fff',
                    fontFamily: 'IRANSans_Light',
                    fontSize: 15,
                }
            }
            );
        }
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Content theme={barghtheme} style={styles.mainWrapper} keyboardShouldPersistTaps='always'>
                    <Modal styles={{
                            flex: 1,
                            backgroundColor: '#000'
                        }} animationType={"fade"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {
                            this.setState({modalVisible: false});
                        }}>
                        <Container style={styles.authModalWrapper}>
                            <View style={{
                                    padding: 10,
                                    backgroundColor: '#fff',
                                    alignItems: 'flex-end',
                                    width: 300,
                                    height: 150
                                }}>
                                <Row>
                                    <Text style={[
                                            styles.fontSans, {
                                                fontSize: 13
                                            }
                                        ]}>{this.state.title}</Text>
                                </Row>
                                <Row style={{
                                        alignSelf: 'center',
                                        marginTop: 0
                                    }}>
                                    <InputGroup borderType='regular' style={[{
                                                borderRadius: 3,
                                                backgroundColor: '#ecf0f1',
                                                width: 280,
                                                height: 35
                                            }
                                        ]}>
                                        <Input value={this.state.inputValue} onChangeText={(text) => {
                                                this.setState({inputValue: text});
                                            }}/>
                                    </InputGroup>
                                </Row>
                                <Row style={{
                                        alignSelf: 'flex-start'
                                    }}>
                                    <Button onPress={() => {
                                            this.setState({modalVisible: false});
                                        }} transparent="transparent" iconRight="iconRight" style={{
                                            padding: 0,
                                            margin: 0,
                                            width: 120
                                        }} textStyle={{
                                            fontFamily: 'IRANSans_Medium',
                                            marginRight: -30,
                                            fontSize: 11
                                        }}>
                                        <Text>انصراف</Text>
                                        <Icon name='close'/>
                                    </Button>
                                    <Button onPress={() => {
                                            this.confirm();
                                        }} transparent="transparent" iconRight="iconRight" style={{
                                            padding: 0,
                                            margin: 0,
                                            width: 120,
                                            marginLeft: 40
                                        }} textStyle={{
                                            fontFamily: 'IRANSans_Medium',
                                            marginRight: -30,
                                            fontSize: 11
                                        }}>
                                        <Text >تایید</Text>
                                        <Icon name='check'/>
                                    </Button>
                                </Row>
                            </View>
                        </Container>
                    </Modal>
                    <Header style={[{
                                flexDirection: 'row'
                            }
                        ]}>
                        <Button onPress={() => {
                                Actions.Home();
                            }} iconLeft="iconLeft" transparent="transparent">
                            <Icon name='chevron-left'/>
                            <Text>بازگشت</Text>
                        </Button>
                        <Title style={[
                                styles.fontSans, {
                                    color: '#fff',
                                    textAlign: 'center',
                                    alignSelf: 'flex-end',
                                    fontSize: 16
                                }
                            ]}>ویرایش پروفایل</Title>
                    </Header>
                    <Text style={[styles.fontSans, styles.wrapperHeader]}>اطلاعات کاربری</Text>
                    <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center'
                            }
                        ]}>
                        <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                            <List style={[{
                                        padding: 0
                                    }
                                ]}>
                                <ListItem>
                                    <Col>
                                        <TouchableWithoutFeedback onPress={() => {
                                                this.edit('last_name')
                                            }}>
                                            <View>
                                                <Text style={[
                                                        styles.fontSans, {
                                                            color: '#5c4b8c',
                                                            fontSize: 12
                                                        }
                                                    ]}>نام خانوادگی</Text>
                                                <Text style={[
                                                        styles.fontSans, {
                                                            fontSize: 13
                                                        }
                                                    ]}>{this.state.last_name}</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </Col>
                                    <Col>
                                        <TouchableWithoutFeedback onPress={() => {
                                                this.edit('first_name')
                                            }}>
                                            <View>
                                                <Text style={[
                                                        styles.fontSans, {
                                                            color: '#5c4b8c',
                                                            fontSize: 12
                                                        }
                                                    ]}>نام</Text>
                                                <Text style={[
                                                        styles.fontSans, {
                                                            fontSize: 13
                                                        }
                                                    ]}>{this.state.first_name}</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </Col>
                                </ListItem>

                                <ListItem>
                                    <Col>
                                        <TouchableWithoutFeedback onPress={() => {
                                                this.edit('email')
                                            }}>
                                            <View>
                                                <Text style={[
                                                        styles.fontSans, {
                                                            color: '#5c4b8c',
                                                            fontSize: 12
                                                        }
                                                    ]}>پست الکترونیک</Text>
                                                <Text style={[styles.fontSans]}>{this.state.email}</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </Col>
                                </ListItem>
                                <ListItem>
                                    <Col>
                                        <TouchableWithoutFeedback onPress={() => {
                                                this.edit('mobile')
                                            }}>
                                            <View>
                                                <Text style={[
                                                        styles.fontSans, {
                                                            color: '#5c4b8c',
                                                            fontSize: 12
                                                        }
                                                    ]}>شماره موبایل</Text>
                                                <Text style={[styles.fontSans]}>{this.state.mobile}</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </Col>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>

                    <Text style={[styles.fontSans, styles.wrapperHeader]}>تغییر رمز عبور</Text>
                    <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center',
                                backgroundColor: 'rgb(245, 245, 245)'
                            }
                        ]}>
                        <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                            <List style={[{
                                        padding: 0
                                    }
                                ]}>
                                <ListItem>
                                    <Col>
                                        <View>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        color: '#5c4b8c',
                                                        fontSize: 12
                                                    }
                                                ]}>رمز عبور فعلی</Text>
                                            <Input style={[styles.fontSans]} secureTextEntry={true} placeholder="******" onChangeText={(text) => {
                                                    this.setState({oldPassword: text});
                                                }}/>
                                        </View>
                                    </Col>
                                </ListItem>
                                <ListItem>
                                    <Col>
                                        <View>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        color: '#5c4b8c',
                                                        fontSize: 12
                                                    }
                                                ]}>تکرار رمز عبور</Text>
                                            <Input style={[styles.fontSans]} secureTextEntry={true} placeholder="******" onChangeText={(text) => {
                                                    this.setState({newPassword: text});
                                                }}/>
                                        </View>
                                    </Col>
                                    <Col>
                                        <View>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        color: '#5c4b8c',
                                                        fontSize: 12
                                                    }
                                                ]}>رمز عبور جدید</Text>
                                            <Input style={[styles.fontSans]} secureTextEntry={true} placeholder="******" onChangeText={(text) => {
                                                    this.setState({confirmPassword: text});
                                                }}/>
                                        </View>
                                    </Col>
                                </ListItem>
                                <ListItem style={[{
                                            backgroundColor: '#5c4b8c',
                                            width: width * 0.97,
                                            padding: 0
                                        }
                                    ]}>
                                    <Button onPress={() => {
                                            this.changePassword();
                                        }} transparent="transparent" style={[{
                                                alignSelf: 'center',
                                                width: width * 0.97,
                                                height: 40
                                            }
                                        ]}>
                                        <Text style={[
                                                styles.fontSans, {
                                                    color: '#fff',
                                                    fontSize: 13
                                                }
                                            ]}>تغییر رمز عبور</Text>
                                    </Button>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}
