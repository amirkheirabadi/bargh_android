import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Row,
    Footer,
    Grid
} from 'native-base';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    View,
    TouchableWithoutFeedback,
    Dimensions,
    BackAndroid,
    Keyboard
} from 'react-native';
import SmsListener from 'react-native-android-sms-listener'; 
import Toast from 'react-native-root-toast';
import {Bubbles, DoubleBounce, Bars, Pulse} from 'react-native-loader';
import Api from '../helper/api';

var styles = require('../style.js');
export default class SignupConfirmation extends Component {
    constructor(props) {
        super(props);
        console.log(this.props);
        this.state = {
            mobile: this.props.data.mobile,
            email: this.props.data.email,
            type: this.props.data.type,
            confirmation: ''
        };
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
                Actions.Home();
                return true;
            }
        );

        if (this.state.mobile) {
            let subscription = SmsListener.addListener(message => {
                let verificationCodeRegex = /کد فعال سازی شما برای ثبت نام در برق چین : [\d]{5}/;
                
                if (verificationCodeRegex.test(message.body)) {
                    let verificationCode = message.body.match(/[\d]{5}/)[0];
                    this.setState({
                        confirmation: verificationCode
                    });
                    subscription.remove();
                    this.check();
                }
            })

        }

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
            () => {
            this.setState({keyBoardShow: true});
        }
        );
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
            () => {
            this.setState({keyBoardShow: false});
        }
        );
    }

    componentWillUnmount() {
        this
            .keyboardDidShowListener
            .remove();
        this
            .keyboardDidHideListener
            .remove();
    }

    async check() {
        if (!this.state.confirmation) {
            Toast.show('لطفا همه فیلد ها را تکمیل نمایید .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#e74c3c',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle: {
                    color: '#fff',
                    fontFamily: 'IRANSans_Light'
                }
            }
            );
            return;
        }
        Keyboard.dismiss();
        this.setState({onWorking: true});
        let response = await Api.request('auth/check',
            'POST', {
            mobile: this.state.mobile,
            email: this.state.email,
            type: this.state.type,
            confirmation: this.state.confirmation
        }
        );

        if (response[0] == 200) {
            Actions.SignupComplete({
                "data": {
                    mobile: this.state.mobile,
                    confirmation: this.state.confirmation,
                    email: this.state.email,
                    type: this.state.type
                }
            });
        }
        this.setState({onWorking: false});
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container style={styles.authWapper}>
                <Image source={require('../resource/images/bg_main.png')} style={[{
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}>
                    <Content theme={barghtheme}  keyboardShouldPersistTaps='always'>
                     <Header style={[{
                                    backgroundColor: 'rgba(255,255,255,0.1)',
                                    height: 60
                                }
                            ]}>
                            <Button onPress={() => {
                                    Actions.Home()
                                }} iconLeft="iconLeft" transparent="transparent">
                                <Icon name='chevron-left'/>
                                <Text>بازگشت</Text>
                            </Button>
                            <Title></Title>
                            <Button transparent="transparent">
                                <Text></Text>
                            </Button>
                        </Header>
                        <View style={[{
                                    height: height - 140
                                }
                            ]}>
                            {
                                !this.state.keyBoardShow
                                    ? (
                                        <View style={[{
                                                    alignSelf: 'center',
                                                    marginTop: height * .1
                                                }
                                            ]}>
                                            <Image source={require('../resource/images/logo_main.png')} style={[{
                                                        width: 120,
                                                        height: 136
                                                    }
                                                ]}/>
                                        </View>
                                    )
                                    : (
                                        <View></View>
                                    )
                            }
                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * 0.8,
                                        marginTop: height * 0.1
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            color: '#fff',
                                            alignSelf: 'center',
                                            textAlign: 'center',
                                            fontSize: 12
                                        }
                                    ]}>لطفا کد فعال سازی که برای شما ارسال شده است را در باکس زیر وارد کنید .</Text>
                            </View>
                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * 0.8,
                                        marginTop: height * 0.05
                                    }
                                ]}>
                                <InputGroup iconRight="iconRight" style={[
                                        styles.authInputs, {
                                            alignSelf: 'center'
                                        }
                                    ]}>
                                    <Icon name='vpn-key' style={{
                                            color: '#fff'
                                        }}/>
                                    <Input style={[styles.fontSans, styles.authInputText]} keyboardType="numeric" value={this.state.confirmation} placeholderTextColor="white" placeholder='کد فعال سازی' onChangeText={(text) => {
                                            this.setState({confirmation: text});
                                        }}/>
                                </InputGroup>
                            </View>

                            {
                                this.state.onWorking
                                    ? (
                                        <View style={[{
                                                    alignSelf: 'center',
                                                    alignItems: 'center',
                                                    marginTop: 30,
                                                    flexDirection: 'row'
                                                }
                                            ]}>
                                            <Bubbles size={13} color="#FF4C74"/>
                                        </View>
                                    )
                                    : (
                                        <Button onPress={() => {
                                                this.check()
                                            }} transparent="transparent" textStyle={[styles.fontSans, styles.authButtonText]} style={[
                                                styles.authButton, {
                                                    alignSelf: 'center',
                                                    marginTop: 30,
                                                    width: width * 0.8
                                                }
                                            ]}>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        color: '#fff'
                                                    }
                                                ]}>
                                                بررسی کد فعال سازی
                                            </Text>
                                        </Button>
                                    )
                            }
                        </View>
                    </Content>
                </Image>
            </Container>
        );
    }
}
