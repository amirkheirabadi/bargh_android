import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Row,
    Footer,
} from 'native-base';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    View,
    TouchableWithoutFeedback,
    Dimensions,
    BackAndroid,
    Keyboard,
    ScrollView
} from 'react-native';
import Toast from 'react-native-root-toast';
import {Bubbles, DoubleBounce, Bars, Pulse} from 'react-native-loader';
import Api from '../helper/api';
import SmsListener from 'react-native-android-sms-listener';

var styles = require('../style.js');
export default class Signin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            input: '',
            password: '',
            modalVisible: false,

            onWorking: false,
            keyBoardShow: false
        };
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
                Actions.Home();
                return true;
            }
        );

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
            () => {
            this.setState({keyBoardShow: true});
        }
        );
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
            () => {
            this.setState({keyBoardShow: false});
        }
        );
    }

    componentWillMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
                Actions.Home();
                return true;
            }
        );
    }


    componentWillUnmount() {
        this
            .keyboardDidShowListener
            .remove();
        this
            .keyboardDidHideListener
            .remove();
    }

    async signin() {
        console.log('run');
        if (!this.state.input || !this.state.password) {
            Toast.show('لطفا همه فیلد ها را تکمیل نمایید .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#e74c3c',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle : {color : '#fff',
                fontFamily: 'IRANSans_Light'}
            });
            return ;
        }
        this.setState({onWorking: true});
        let response = await Api.request('auth/signin',
            'POST', {
            input: this.state.input,
            password: this.state.password
        }
        );
        if (response[0] == 200) {
            let store = await Api.userStore(JSON.stringify(response[1]['data']));
            Actions.Home();
        }
        this.setState({onWorking: false});
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
                  <View>
                      <ScrollView keyboardShouldPersistTaps="handled" >

                                    <Input blurOnSubmit={false} style={[styles.fontSans, styles.authInputText]} placeholderTextColor="white" placeholder='شماره موبایل و یا پست الکترونیک' onChangeText={(text) => {
                                            this.setState({input: text});
                                        }}/>
 <TouchableWithoutFeedback onPress={() => {
                                                this.signin()
                                            }} transparent="transparent" textStyle={[styles.fontSans, styles.authButtonText]} style={[
                                                styles.authButton, {
                                                    alignSelf: 'center',
                                                    marginTop: 30,
                                                    width: width * 0.8
                                                }
                                            ]}>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        color: '#fff'
                                                    }
                                                ]}>
                                                ورود به حساب کاربری
                                            </Text>
                                        </TouchableWithoutFeedback>
                                                        </ScrollView>
                                                        </View>           
        );
    }
}
