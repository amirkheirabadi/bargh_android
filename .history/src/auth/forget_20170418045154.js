import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Row,
    Col,
    Footer
} from 'native-base';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    View,
    Modal,
    TouchableWithoutFeedback,
    Dimensions,
    BackAndroid,
    Keyboard
} from 'react-native';
import Toast from 'react-native-root-toast';
import {Bubbles, DoubleBounce, Bars, Pulse} from 'react-native-loader';
import Api from '../helper/api';

var styles = require('../style.js');
export default class Forget extends Component {
    constructor(props) {
        super(props);

        this.state = {
            input: '',
            modalVisible: false,

            onWorking: false,
            keyBoardShow: false
        };
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
                Actions.Signin();
                return true;
            }
        );

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
            () => {
            this.setState({keyBoardShow: true});
        }
        );
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
            () => {
            this.setState({keyBoardShow: false});
        }
        );
    }

    componentWillUnmount() {
        this
            .keyboardDidShowListener
            .remove();
        this
            .keyboardDidHideListener
            .remove();
    }

    showModal(visible) {
        if (visible) {
            if (!this.state.input) {
                Toast.show('لطفا همه فیلد ها را تکمیل نمایید .', {
                    duration: Toast.durations.LONG,
                    position: -50,
                    backgroundColor: '#e74c3c',
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    textStyle : {color : '#fff', fontFamily: 'IRANSans_Light'}
                });
                return ;
            }
        }
        this.setState({modalVisible: visible});
    }

    async forget() {
        this.setState({
            modalVisible: false,
            onWorking: true
        });
        let response = await Api.request('auth/forget',
            'POST', {input: this.state.input}
        );
        if (response[0] == 200) {
            Actions.ForgetConfirmation({input: this.state.input})
        }
        this.showModal(false);
        this.setState({onWorking: false});
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container style={styles.authWapper}  keyboardShouldPersistTaps='always'>
                <Image source={require('../resource/images/bg_main.png')} style={[{
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}>
                    <Content theme={barghtheme}>
                     <Header style={[{
                                    backgroundColor: 'rgba(255,255,255,0.1)',
                                    height: 60
                                }
                            ]}>
                            <Button onPress={() => {
                                    Actions.Signin()
                                }} iconLeft="iconLeft" transparent="transparent">
                                <Icon name='chevron-left'/>
                                <Text>بازگشت</Text>
                            </Button>
                            <Title></Title>
                            <Button transparent="transparent">
                                <Text></Text>
                            </Button>
                        </Header>
                        <Modal styles={{
                                flex: 1,
                                backgroundColor: '#000'
                            }} animationType={"fade"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {
                                this.showModal(!this.state.modalVisible)
                            }}>
                            <Container style={styles.authModalWrapper}>
                                <View style={styles.authModal}>
                                    <Row>
                                        <Text style={[
                                                styles.fontSans, {
                                                    fontSize: 15
                                                }
                                            ]}>تایید اطلاعات</Text>
                                    </Row>
                                    <Row>
                                        <Text style={[
                                                styles.fontSans, {
                                                    fontSize: 13
                                                }
                                            ]}>در صورت تایید ایمیل و یا شماره موبایل ، ایمیل و یا پیامکی جهت بازیابی رمز عبور برای شما ارسال خواهد شد .</Text>
                                    </Row>
                                    <Row style={{
                                            alignSelf: 'center',
                                            marginTop: 0
                                        }}>
                                        <Text style={[
                                                styles.fontSans, {
                                                    fontSize: 13,
                                                    color: '#5c4b8c'
                                                }
                                            ]}>{this.state.input}</Text>
                                    </Row>
                                    <Row>
                                        <Button onPress={() => {
                                                this.showModal(false)
                                            }} transparent="transparent" iconRight="iconRight" style={{
                                                padding: 0,
                                                margin: 0,
                                                width: 120
                                            }} textStyle={{
                                                fontFamily: 'IRANSans_Medium',
                                                marginRight: -30,
                                                fontSize: 11
                                            }}>
                                            <Text>انصراف</Text>
                                            <Icon name='close'/>
                                        </Button>
                                        <Button onPress={() => {
                                                this.forget()
                                            }} transparent="transparent" iconRight="iconRight" style={{
                                                padding: 0,
                                                margin: 0,
                                                width: 120,
                                                marginLeft: 40
                                            }} textStyle={{
                                                fontFamily: 'IRANSans_Medium',
                                                marginRight: -30,
                                                fontSize: 11
                                            }}>
                                            <Text >تایید</Text>
                                            <Icon name='check'/>
                                        </Button>
                                    </Row>
                                </View>
                            </Container>
                        </Modal>

                        <View style={[{
                                    height: height - 80
                                }
                            ]}>
                            {
                                !this.state.keyBoardShow
                                    ? (
                                        <View style={[{
                                                    alignSelf: 'center',
                                                    marginTop: height * .1
                                                }
                                            ]}>
                                            <Image source={require('../resource/images/logo_main.png')} style={[{
                                                        width: 120,
                                                        height: 136
                                                    }
                                                ]}/>
                                        </View>
                                    )
                                    : (
                                        <View></View>
                                    )
                            }
                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * 0.8,
                                        marginTop: height * 0.1
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            color: '#fff',
                                            alignSelf: 'center',
                                            textAlign: 'center',
                                            fontSize: 12
                                        }
                                    ]}>شماره تلفن و یا ایمیل کاربری که قبلا در سیستم ثبت شده است را وارد کنید تا کد بازیابی رمز عبور برای شما ارسال شود .</Text>
                            </View>
                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * 0.8,
                                        marginTop: height * 0.05
                                    }
                                ]}>
                                <InputGroup iconRight="iconRight" style={[
                                        styles.authInputs, {
                                            alignSelf: 'center'
                                        }
                                    ]}>
                                    <Icon name='vpn-key' style={{
                                            color: '#fff'
                                        }}/>
                                    <Input style={[styles.fontSans, styles.authInputText]} placeholderTextColor="white" placeholder='شماره موبایل یا پست الکترونیک' onChangeText={(text) => {
                                            this.setState({input: text});
                                        }}/>
                                </InputGroup>
                            </View>

                               {
                                this.state.onWorking
                                    ? (
                                        <View style={[{
                                                    alignSelf: 'center',
                                                    alignItems: 'center',
                                                    marginTop: 30,
                                                    flexDirection: 'row'
                                                }
                                            ]}>
                                            <Bubbles size={13} color="#FF4C74"/>
                                        </View>
                                    )
                                    : (
                                        <Button onPress={() => {
                                    this.showModal(!this.state.modalVisible)
                                }} transparent="transparent" textStyle={[styles.fontSans, styles.authButtonText]} style={[
                                    styles.authButton, {
                                        alignSelf: 'center',
                                        marginTop: 20,
                                        width: width * 0.8
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            color: '#fff'
                                        }
                                    ]}>
                                    بازیابی رمز عبور
                                </Text>
                            </Button>
                                    )
                            }                         
                        </View>
                        <Footer style={[{
                                backgroundColor: 'rgba(255,255,255,.1)',
                                paddingTop: 3,
                                height: 60
                            }
                        ]}>
                        <TouchableWithoutFeedback onPress={() => {
                                Actions.Signin()
                            }} underlayColor="#5c4b8c">
                            <View>
                                <Text style={[
                                        styles.fontSans, {
                                            color: '#fff',
                                            textAlign: 'center'
                                        }
                                    ]}>ورود به حساب کاربری</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </Footer>
                    </Content>
                </Image>
            </Container>
        );
    }
}
