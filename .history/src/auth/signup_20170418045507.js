import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Row,
    Footer,
    Col,
    Grid
} from 'native-base';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    Modal,
    View,
    TouchableWithoutFeedback,
    Dimensions,
    BackAndroid,
    Keyboard
} from 'react-native';
import Toast from 'react-native-root-toast';
import {Bubbles, DoubleBounce, Bars, Pulse} from 'react-native-loader';
import Api from '../helper/api';

var styles = require('../style.js');
export default class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mobile: '',
            last_name: '',
            first_name: '',
            type: 'mobile',
            modalVisible: false,

            onWorking: false,
            keyBoardShow: false
        };

    }

    showModal(visible) {
        if (visible) {
            if (!this.state.mobile || !this.state.first_name || !this.state.last_name) {
                Toast.show('لطفا همه فیلد ها را تکمیل نمایید .', {
                    duration: Toast.durations.LONG,
                    position: -50,
                    backgroundColor: '#e74c3c',
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    textStyle: {
                        color: '#fff',
                        fontFamily: 'IRANSans_Light'
                    }
                }
                );
                return;
            }
        }
        this.setState({modalVisible: visible});
    }

    async register() {
        Keyboard.dismiss();
        this.setState({modalVisible: false, onWorking: true});
        let response = await Api.request('auth/signup',
            'POST', {
            mobile: this.state.mobile,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            type: 'mobile'
        }
        );
        if (response[0] == 200) {
            Actions.SignupConfirmation({
                'data': {
                    mobile: this.state.mobile,
                    'type': this.state.type
                }
            });
        }
        this.setState({onWorking: false});
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
                Actions.pop();
                return true;
            }
        );

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
            () => {
            this.setState({keyBoardShow: true});
        }
        );
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
            () => {
            this.setState({keyBoardShow: false});
        }
        );
    }

    componentWillUnmount() {
        this
            .keyboardDidShowListener
            .remove();
        this
            .keyboardDidHideListener
            .remove();
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container style={styles.authWapper}>
                <Image source={require('../resource/images/bg_main.png')} style={[{
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}>
                    <Content theme={barghtheme}  keyboardShouldPersistTaps='always'>
                        <Modal styles={{
                                flex: 1,
                                backgroundColor: '#000'
                            }} animationType={"fade"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {
                                this.showModal(!this.state.modalVisible)
                            }}>
                            <Container style={styles.authModalWrapper}>
                                <View style={styles.authModal}>
                                    <Row>
                                        <Text style={[
                                                styles.fontSans, {
                                                    fontSize: 15
                                                }
                                            ]}>تایید شماره تلفن</Text>
                                    </Row>
                                    <Row>
                                        <Text style={[
                                                styles.fontSans, {
                                                    fontSize: 13
                                                }
                                            ]}>در صورت تایید شماره تلفن همراه پیامکی حاوی کد فعال سازی برای شما ارسال خواهد شد .</Text>
                                    </Row>
                                    <Row style={{
                                            alignSelf: 'center',
                                            marginTop: 0
                                        }}>
                                        <Text style={[
                                                styles.fontSans, {
                                                    fontSize: 17,
                                                    color: '#5c4b8c'
                                                }
                                            ]}>{this.state.mobile}</Text>
                                    </Row>
                                    <Row>
                                        <Button onPress={() => {
                                                this.showModal(false)
                                            }} transparent="transparent" iconRight="iconRight" style={{
                                                padding: 0,
                                                margin: 0,
                                                width: 120
                                            }} textStyle={{
                                                fontFamily: 'IRANSans_Medium',
                                                marginRight: -30,
                                                fontSize: 11
                                            }}>
                                            <Text>انصراف</Text>
                                            <Icon name='close'/>
                                        </Button>
                                        <Button onPress={() => {
                                                this.register()
                                            }} transparent="transparent" iconRight="iconRight" style={{
                                                padding: 0,
                                                margin: 0,
                                                width: 120,
                                                marginLeft: 40
                                            }} textStyle={{
                                                fontFamily: 'IRANSans_Medium',
                                                marginRight: -30,
                                                fontSize: 11
                                            }}>
                                            <Text >تایید</Text>
                                            <Icon name='check'/>
                                        </Button>
                                    </Row>
                                </View>
                            </Container>
                        </Modal>
                        <Header style={[{
                                    backgroundColor: 'rgba(255,255,255,0.1)',
                                    height: 60
                                }
                            ]}>
                            <Button transparent="transparent">
                                <Text></Text>
                            </Button>
                            <Title></Title>
                            <Button onPress={() => {
                                    Actions.SignupEmail()
                                }} iconRight="iconRight" transparent="transparent">
                                <Icon name='chevron-right'/>
                                <Text>ثبت نام با پست الکترونیک</Text>
                            </Button>
                        </Header>
                        <View style={[{
                                    height: height - 140
                                }
                            ]}>
                            {
                                !this.state.keyBoardShow
                                    ? (
                                        <View style={[{
                                                    alignSelf: 'center',
                                                    marginTop: height * .1
                                                }
                                            ]}>
                                            <Image source={require('../resource/images/logo_main.png')} style={[{
                                                        width: 120,
                                                        height: 136
                                                    }
                                                ]}/>
                                        </View>
                                    )
                                    : (
                                        <View></View>
                                    )
                            }
                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * .8,
                                        marginTop: height * .05
                                    }
                                ]}>
                                <InputGroup iconRight="iconRight" style={[
                                        styles.authInputs, {
                                            alignSelf: 'center'
                                        }
                                    ]}>
                                    <Icon name='phone-android' style={{
                                            color: '#fff'
                                        }}/>
                                    <Input style={[styles.fontSans, styles.authInputText]} placeholderStyle={styles.fontSans} keyboardType="numeric" placeholderTextColor="white" value={this.state.mobile} placeholder='شماره موبایل (*******091)' onChangeText={(text) => {
                                            this.setState({mobile: text});
                                        }}/>
                                </InputGroup>
                            </View>

                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * 0.8,
                                        marginTop: 20
                                    }
                                ]}>
                                <InputGroup iconRight="iconRight" style={[
                                        styles.authInputs, {
                                            alignSelf: 'center'
                                        }
                                    ]}>
                                    <Icon name='person' style={{
                                            color: '#fff'
                                        }}/>
                                    <Input style={[styles.fontSans, styles.authInputText]} placeholderTextColor="white" placeholder='نام' value={this.state.first_name} onChangeText={(text) => {
                                            this.setState({first_name: text});
                                        }}/>
                                </InputGroup>
                            </View>
                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * 0.8,
                                        marginTop: 10
                                    }
                                ]}>
                                <InputGroup iconRight="iconRight" style={[
                                        styles.authInputs, {
                                            alignSelf: 'center'
                                        }
                                    ]}>
                                    <Icon name='person' style={{
                                            color: '#fff'
                                        }}/>
                                    <Input style={[styles.fontSans, styles.authInputText]} placeholderTextColor="white" placeholder='نام خانوادگی' value={this.state.last_name} onChangeText={(text) => {
                                            this.setState({last_name: text});
                                        }}/>
                                </InputGroup>
                            </View>
                            <Button onPress={() => {
                                    Actions.Rules();
                                }} transparent="transparent" style={[{
                                        marginTop: 20,
                                        alignSelf: 'center'
                                    }
                                ]}>
                                <Text underlineColorAndroid={"blue"} style={[
                                        styles.fontSans, {
                                            color: '#fff',
                                            textAlign: 'center',
                                            textDecorationLine: 'underline'
                                        }
                                    ]}>قوانین برق چین</Text>
                            </Button>

                            {
                                this.state.onWorking
                                    ? (
                                        <View style={[{
                                                    alignSelf: 'center',
                                                    alignItems: 'center',
                                                    marginTop: 30,
                                                    flexDirection: 'row'
                                                }
                                            ]}>
                                            <Bubbles size={13} color="#FF4C74"/>
                                        </View>
                                    )
                                    : (
                                        <Button onPress={() => {
                                                this.showModal(!this.state.modalVisible)
                                            }} transparent="transparent" textStyle={[styles.fontSans, styles.authButtonText]} style={[
                                                styles.authButton, {
                                                    alignSelf: 'center',
                                                    marginTop: 20,
                                                    width: width * 0.8
                                                }
                                            ]}>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        color: '#fff'
                                                    }
                                                ]}>
                                                ثبت نام
                                            </Text>
                                        </Button>
                                    )
                            }
                        </View>
                        <Footer style={[{
                                    backgroundColor: 'rgba(255,255,255,.1)',
                                    paddingTop: 3,
                                    height: 60
                                }
                            ]}>
                            <TouchableWithoutFeedback onPress={() => {
                                    Actions.Signin()
                                }} underlayColor="#5c4b8c">
                                <View>
                                    <Text style={[
                                            styles.fontSans, {
                                                color: '#fff',
                                                textAlign: 'center'
                                            }
                                        ]}>قبلا در سیستم ثبت نام کرده اید ؟</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </Footer>
                    </Content>
                </Image>
            </Container>
        );
    }
}
