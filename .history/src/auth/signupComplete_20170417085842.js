import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Row,
    Footer,
    Grid
} from 'native-base';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    View,
    TouchableWithoutFeedback,
    Dimensions,
    BackAndroid,
    Keyboard
} from 'react-native';
import Toast from 'react-native-root-toast';
import {Bubbles, DoubleBounce, Bars, Pulse} from 'react-native-loader';
import Api from '../helper/api';

var styles = require('../style.js');
export default class SignupComplete extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mobile: this.props.data.mobile,
            email: this.props.data.email,
            type: this.props.data.type,
            confirmation: this.props.data.confirmation,
            password: '',
            password_confirmation: '',

            onWorking: false,
            keyBoardShow: false
        };
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
            Actions.Home();
            return true;
        }
        );

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
            () => {
            this.setState({keyBoardShow: true});
        }
        );
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
            () => {
            this.setState({keyBoardShow: false});
        }
        );
    }

    componentWillUnmount() {
        this
            .keyboardDidShowListener
            .remove();
        this
            .keyboardDidHideListener
            .remove();
    }

    async complete() {
        if (!this.state.password_confirmation || !this.state.password) {
            Toast.show('لطفا همه فیلد ها را تکمیل نمایید .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#e74c3c',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle: {
                    color: '#fff',
                    fontFamily: 'IRANSans_Light'
                }
            }
            );
            return;
        }
        this.setState({onWorking: true});
        let response = await Api.request('auth/compilation',
            'POST', {
            mobile: this.state.mobile,
            email: this.state.email,
            type: this.state.type,
            confirmation: this.state.confirmation,
            password: this.state.password,
            password_confirmation: this.state.password_confirmation
        }
        );
        if (response[0] == 200) {
            Toast.show('ثبت نام شما با موفقیت انجام شد .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#2ecc71',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle: {
                    color: '#fff',
                    fontFamily: 'IRANSans_Light'
                }
            }
            );
            let store = await Api.userStore(JSON.stringify(response[1]['data']));
            Actions.Home();
        }
        this.setState({onWorking: false});
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container style={styles.authWapper}>
                <Image source={require('../resource/images/bg_main.png')} style={[{
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}>
                    <Content theme={barghtheme}>
                        <View style={[{
                                    height: height - 20
                                }
                            ]}>
                            {
                                !this.state.keyBoardShow
                                    ? (
                                        <View style={[{
                                                    alignSelf: 'center',
                                                    marginTop: height * .1
                                                }
                                            ]}>
                                            <Image source={require('../resource/images/logo_main.png')} style={[{
                                                        width: 120,
                                                        height: 136
                                                    }
                                                ]}/>
                                        </View>
                                    )
                                    : (
                                        <View></View>
                                    )
                            }
                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * 0.8,
                                        marginTop: height * 0.1
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            color: '#fff',
                                            alignSelf: 'center',
                                            textAlign: 'center',
                                            fontSize: 12
                                        }
                                    ]}>برای مرحله نهایی ثبت نام کافیست رمز عبور خود را باکس زیر وارد کرده تا ثبت نام شما کامل شود .</Text>
                            </View>
                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * 0.8,
                                        marginTop: 30
                                    }
                                ]}>
                                <InputGroup iconRight="iconRight" style={[
                                        styles.authInputs, {
                                            alignSelf: 'center'
                                        }
                                    ]}>
                                    <Icon name='vpn-key' style={{
                                            color: '#fff'
                                        }}/>
                                    <Input style={[styles.fontSans, styles.authInputText]} secureTextEntry={true} placeholderTextColor="white" placeholder='رمز عبور' onChangeText={(text) => {
                                            this.setState({password: text});
                                        }}/>
                                </InputGroup>
                            </View>

                            <View style={[{
                                        alignSelf: 'center',
                                        width: width * 0.8,
                                        marginTop: 10
                                    }
                                ]}>
                                <InputGroup iconRight="iconRight" style={[
                                        styles.authInputs, {
                                            alignSelf: 'center'
                                        }
                                    ]}>
                                    <Icon name='vpn-key' style={{
                                            color: '#fff'
                                        }}/>
                                    <Input style={[styles.fontSans, styles.authInputText]} secureTextEntry={true} placeholderTextColor="white" placeholder='تکرار رمز عبور' onChangeText={(text) => {
                                            this.setState({password_confirmation: text});
                                        }}/>
                                </InputGroup>
                            </View>

                            {
                                this.state.onWorking
                                    ? (
                                        <View style={[{
                                                    alignSelf: 'center',
                                                    alignItems: 'center',
                                                    marginTop: 30,
                                                    flexDirection: 'row'
                                                }
                                            ]}>
                                            <Bubbles size={13} color="#FF4C74"/>
                                        </View>
                                    )
                                    : (
                                        <Button onPress={() => {
                                                this.complete()
                                            }} transparent="transparent" textStyle={[styles.fontSans, styles.authButtonText]} style={[
                                                styles.authButton, {
                                                    alignSelf: 'center',
                                                    marginTop: 30,
                                                    width: width * 0.8
                                                }
                                            ]}>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        color: '#fff'
                                                    }
                                                ]}>
                                                تکمیل ثبت نام
                                            </Text>
                                        </Button>
                                    )
                            }
                        </View>
                    </Content>
                </Image>
            </Container>
        );
    }
}
