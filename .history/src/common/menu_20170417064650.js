import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import Api from '../helper/api';

import {Container, Content, Grid, Row, Text} from 'native-base';
import {
    Image,
    Dimensions,
    TouchableWithoutFeedback,
    View
} from 'react-native';

var styles = require('../style.js');
export default class Menu extends Component{

    constructor(props) {
        super(props);
        this.state = {
            user: '',
        };
    }

    async componentWillMount () {
        let user = await Api.userFetch();
        if (user) {
            this.setState({user : JSON.parse(user)});
        }
    }

    signout() {
        this.setState({user:''});
        this.props.signout();
    }

    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        let user = null;
        if (typeof this.state.user == "object") {
            user = true;
        } else {
            user =false;
        }
        return (
            <Container style={styles.menuWapper}>
                <Content>
                    <Grid>
                        <Row style={[{
                                alignSelf: 'center',
                            }
                        ]}>
                            <Image resizeMode="cover" source={require('../resource/images/logo_main.png')} style={[{
                                    height: 150,
                                    width: (width / 2) + 50
                                }]} />
                        </Row>

                        {this.state.user ? (
                            <Row onPress={() => {Actions.Settings()}}  style={[styles.menuItems, styles.menuUser]}>
                                <Text style={[styles.fontSans,{
                                        color:'#fff',
                                        fontSize: 14,
                                        paddingTop:24,
                                        paddingRight:20
                                    }]}>
                                    {this.state.user.first_name + ' ' + this.state.user.last_name}
                                </Text>
                                <Image source={require('../resource/images/menu/male.png')} style={[{
                                        width: 60,
                                        height: 60,
                                        marginTop:7
                                    }
                                ]}/>
                                </Row>
                            ):(
                                <TouchableWithoutFeedback onPress={() => {Actions.Signin()}} >
                                   <Row  style={[styles.menuItems, styles.menuUser]}>
                                    <Text style={[styles.fontSans,{
                                            color:'#fff',
                                            fontSize: 14,
                                            paddingTop:24,
                                            paddingRight:20
                                        }]}>
                                        ورود - ثبت نام
                                    </Text>
                                    <Image source={require('../resource/images/menu/male.png')} style={[{
                                            width: 60,
                                            height: 60,
                                            marginTop:7
                                        }
                                    ]}/>
                                    </Row>
                            </TouchableWithoutFeedback>
                        )}

                        <Row style={[styles.menuItems, styles.otherItems,{
                             paddingTop:10,
                        }]}>
                            <TouchableWithoutFeedback onPress={() => {Actions.Home()}}>
                                <View style={[{flexDirection:'row'}]}>
                                    <Text style={[styles.fontSans,{
                                        fontSize: 12,
                                        paddingTop:5,
                                        paddingRight:20
                                    }]}>
                                    صفحه اصلی
                                </Text>
                                <Image source={require('../resource/images/menu/home.png')} style={[{
                                        width: 35,
                                        height: 35
                                    }
                                ]}/>
                                </View>
                            </TouchableWithoutFeedback>
                        </Row>

                        <Row style={[styles.menuItems, styles.otherItems]}>
                            <TouchableWithoutFeedback onPress={() => {Actions.Home()}}>
                                <View style={[{flexDirection:'row'}]}>
                                    <Text style={[styles.fontSansLight,{
                                        fontSize: 12,
                                        paddingTop:5,
                                        paddingRight:20
                                    }]}>
                                    جستجو
                                </Text>
                                <Image source={require('../resource/images/menu/analytics-1.png')} style={[{
                                        width: 35,
                                        height: 35
                                    }
                                ]}/>
                                </View>
                            </TouchableWithoutFeedback>
                        </Row>

                         <Row style={[styles.menuItems, styles.otherItems]}>
                            <TouchableWithoutFeedback onPress={() => {Actions.Home()}}>
                                <View style={[{flexDirection:'row'}]}>
                                    <Text style={[styles.fontSansLight,{
                                        fontSize: 12,
                                        paddingTop:5,
                                        paddingRight:20
                                    }]}>
                                    ثبت کسب و کار جدید
                                </Text>
                                <Image source={require('../resource/images/menu/shopping-cart.png')} style={[{
                                        width: 35,
                                        height: 35
                                    }
                                ]}/>
                                </View>
                            </TouchableWithoutFeedback>
                        </Row>
                        

                        <Row style={[styles.menuItems, styles.otherItems]}>
                          <TouchableWithoutFeedback onPress={() => {Actions.News()}}>
                          <View style={[{flexDirection:'row'}]}>
                            <Text style={[styles.fontSansLight,{
                                    fontSize: 12,
                                    paddingTop:5,
                                    paddingRight:20
                                }]}>
                                اخبار
                            </Text>
                            <Image source={require('../resource/images/menu/flag.png')} style={[{
                                    width: 35,
                                    height: 35
                                }
                            ]}/>
                            </View>
                          </TouchableWithoutFeedback>
                        </Row>
                     

                        <Row style={[styles.menuItems, styles.otherItems]}>
                          <TouchableWithoutFeedback onPress={() => {Actions.About()}}>
                            <View style={[{flexDirection:'row'}]}>
                            <Text style={[styles.fontSansLight,{
                                    fontSize: 12,
                                    paddingTop:5,
                                    paddingRight:20
                                }]}>
                                درباره ما
                            </Text>
                            <Image source={require('../resource/images/menu/flask.png')} style={[{
                                    width: 35,
                                    height: 35
                                }
                            ]}/>
                            </View>
                          </TouchableWithoutFeedback>
                        </Row>
                        
                         {this.state.user ? (
                            <Row  style={[styles.menuItems, styles.otherItems]}>
                             <TouchableWithoutFeedback onPress={() => {this.signout()}} >
                            <View style={[{flexDirection:'row'}]}>
                                    <Text style={[styles.fontSansLight,{
                                        fontSize: 12,
                                        paddingTop:10,
                                        paddingRight:20
                                    }]}>
                                    خروج از حساب کاربری
                                </Text>
                                <Image source={require('../resource/images/menu/user.png')} style={[{
                                        width: 35,
                                        height: 35,
                                        marginTop:7
                                    }
                                ]}/>
                                 </View>
                          </TouchableWithoutFeedback>
                                </Row>
                            ):(
                                <Row></Row>
                        )}
                    </Grid>
                </Content>
            </Container>
        );
    }
}
