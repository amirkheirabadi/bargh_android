import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import {
    Container,
    Content,
    Icon,
    Button,
    Grid,
    Row
} from 'native-base';
import Toast from 'react-native-root-toast';
import barghtheme from '../theme/barghchin';
import {Text, NetInfo, BackAndroid, Image} from 'react-native';
var styles = require('../style.js');
export default class Offline extends Component {
    constructor(props) {
        super(props);

        this.state = {
            exitTimer: 0,
        };
    }

    checkConnected() {
        NetInfo
            .isConnected
            .fetch()
            .then(isConnected => {
                if (isConnected) {
                    return Actions.Intro();
                }
            });
    }

   componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', () => {
            var time = (new Date()).getTime();
            if (time - this.state.exitTimer > 3000) {
                this.state.exitTimer = time;
                Toast.show('برای خروج از برنامه دوبار کلیک کنید .', {
                    duration: Toast.durations.SHORT,
                    position: -50,
                    backgroundColor: '#34495e',
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    textStyle : {color : '#fff'}
                });
                return true;
            }
            BackAndroid.exitApp();
            return false;
        });
    }

    render() {
        return (
            <Container style={[{
                        backgroundColor: '#fff'
                    }
                ]}>
                <Image source={require('../resource/images/bg_main.png')} style={[{
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}>
                    <Content theme={barghtheme}>
                        <Grid>
                            <Row style={[{
                                        alignSelf: 'center',
                                        paddingTop: 200
                                    }
                                ]}>
                                <Icon name="portable-wifi-off" style={{
                                        fontSize: 100,
                                        color: '#FF4C74'
                                    }}/>
                            </Row>
                            <Row style={[{
                                        alignSelf: 'center',
                                        marginTop: 50
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            'color': '#fff'
                                        }
                                    ]}>خطا در اتصال به اینترنت !</Text>
                            </Row>
                            <Row style={[{
                                        alignSelf: 'center'
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            'color': '#fff',
                                            paddingTop: 15
                                        }
                                    ]}>لطفا وضعیت اینترنت خود را بررسی کنید .</Text>
                            </Row>
                            <Row style={[{
                                        alignSelf: 'center',
                                        marginTop: 60
                                    }
                                ]}>
                                <Button onPress={this
                                        .checkConnected
                                        .bind(this)} info="info">
                                    <Text style={[
                                            styles.fontSans, {
                                                color: '#fff'
                                            }
                                        ]}>
                                        بررسی مجدد</Text>
                                </Button>
                            </Row>
                        </Grid>
                    </Content>
                </Image>
            </Container>

        );
    }
}
