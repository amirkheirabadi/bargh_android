import React, {Component} from 'react';
import {Scene, Router, Actions, Reducer, ActionConst} from 'react-native-router-flux';
import Home from './home/home';
import Signup from './auth/signup';
import SignupEmail from './auth/signupEmail';
import SignupConfirmation from './auth/signupConfirmation';
import SignupComplete from './auth/signupComplete';
import Signin from './auth/signin';
import Forget from './auth/forget';
import ForgetConfirmation from './auth/forgetConfirmation';
import ForgetComplete from './auth/forgetComplete';
import Rules from './rules/rules';
import Offline from './error/offline';
import Loading from './error/loading';
import Search from './search/search';
import About from './about/about';
import Settings from './user/settings';
import ProfileConfirm from './user/profileConfirm';
import Business from './user/business';
import Company from './company/company';
import Producer from './producer/producer';
import CompanyProfile from './user/companyPorfile';
import News from './news/news';
import NewsItem from './news/newItem';
import Map from './helper/map';
import Categories from './helper/categories';
import Brands from './helper/brands';
import City from './helper/city';
import Technicians from './technicians/technicians'
import Products from './helper/products';
import Product from './product/product';
import ProductShow from './product/productShow';
import Importer from './importer/importer';
import Skill from './helper/skills';
import Gallery from './helper/Gallery';
import Comments from './comments/comments';
import Error from './error/error';
import Intro from './intro/intro';
import Upgrade from './helper/upgrade';

const scenes = Actions.create(
    <Scene key="root" animation="fade">

        <Scene key="Loading" component={Loading} hideNavBar="hideNavBar" duration={0}/>

        <Scene key="Intro" component={Intro} hideNavBar="hideNavBar" duration={0} initial/>

        <Scene key="Home" component={Home} hideNavBar="hideNavBar" duration={0} />
        <Scene key="Error" component={Error} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Upgrade" component={Upgrade} hideNavBar="hideNavBar" duration={0}/>

        <Scene key="Rules" component={Rules} hideNavBar="hideNavBar" duration={0}/>

        <Scene key="Forget" component={Forget} hideNavBar="hideNavBar" duration={0} />
        <Scene key="ForgetConfirmation" component={ForgetConfirmation} hideNavBar="hideNavBar" duration={0} />
        <Scene key="ForgetComplete" component={ForgetComplete} hideNavBar="hideNavBar" duration={0} />
        <Scene key="Signup" component={Signup} hideNavBar="hideNavBar" duration={0} />
        <Scene key="SignupEmail" component={SignupEmail} hideNavBar="hideNavBar" duration={0} />
        <Scene key="SignupConfirmation" component={SignupConfirmation} hideNavBar="hideNavBar" duration={0} />
        <Scene key="SignupComplete" component={SignupComplete} hideNavBar="hideNavBar" duration={0} />
        <Scene key="Signin" component={Signin} hideNavBar="hideNavBar" duration={0} />

        <Scene key="Offline" component={Offline} hideNavBar="hideNavBar" duration={0}/>

        <Scene key="Settings" component={Settings} hideNavBar="hideNavBar" duration={0} />
        <Scene key="ProfileConfirm" component={ProfileConfirm} hideNavBar="hideNavBar" duration={0} />

        <Scene key="Business" component={Business} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Company" component={Company} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Importer" component={Importer} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Producer" component={Producer} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Technicians" component={Technicians} hideNavBar="hideNavBar" duration={0}/>

        <Scene key="CompanyProfile" component={CompanyProfile} hideNavBar="hideNavBar" duration={0}/>

        <Scene key="Map" component={Map} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Categories" component={Categories} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Products" component={Products} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Skill" component={Skill} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Brands" component={Brands} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="City" component={City} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="News" component={News} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="NewsItem" component={NewsItem} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="Gallery" component={Gallery} hideNavBar="hideNavBar" direction="vertical" duration={0}/>

        <Scene key="Comments" component={Comments} hideNavBar="hideNavBar" duration={0}/>

        <Scene key="Product" component={Product} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="ProductShow" component={ProductShow} hideNavBar="hideNavBar" duration={0}/>

        <Scene key="Search" component={Search} hideNavBar="hideNavBar" duration={0}/>
        <Scene key="About" component={About} hideNavBar="hideNavBar" duration={0}/>

    </Scene>
);
export default class Main extends Component {
    constructor(opts,
        props
    ) {
        super(opts,
            props
        );
        this.currentScene = 'Intro';
        this.state = {
            currentRoute: 'Intro'
        }
    }

    reducerCreate(params) {
        const defaultReducer = Reducer(params);
        return function (state, action) {
            if (action.type == ActionConst.FOCUS) {
                let scene = action.scene;
                this.currentScene = scene.sceneKey;
            }
            return defaultReducer(state,
                action
            );
        }
    }

    render() {
        return <Router scenes={scenes}
            panHandlers={null}
            />
    }
}
