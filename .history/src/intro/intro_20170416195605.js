import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import SplashScreen from 'react-native-splash-screen';
import Api from '../helper/api';
import DeviceInfo from 'react-native-device-info';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon, 
    List,
    ListItem,
    Picker,
    Item,
    Button,
    Grid,
    Row
} from 'native-base';
import {Bubbles, DoubleBounce, Bars, Pulse} from 'react-native-loader';

import {Text, Image} from 'react-native';
var styles = require('../style.js');
export default class Intro extends Component {

    async componentDidMount() {
        SplashScreen.hide();
        var AppVersion = DeviceInfo.getVersion();

        let response = await Api.request('init',
            'POST', {version: AppVersion}
        );

        if (response[0] != 200) {
            return Actions.Error();
        }
        var results = response[1]['data'];
        if (results['version_link']) {
          return Actions.Upgrade({
            url: results['version_link']
          });
        }

        return Actions.Home();
    }

    render() {
        return (
            <Container style={[{
                        backgroundColor: '#2C333D'
                    }
                ]}>
                <Image source={require('../resource/images/bg_main.png')} style={[{
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}>
                    <Content>
                        <Grid>
                            <Row style={{
                                    alignSelf: 'center',
                                    marginTop: 150
                                }}>
                                <Image source={require('../resource/images/logo_main.png')} style={{
                                        width: 120,
                                        height: 136
                                    }}/>
                            </Row>

                            <Row style={[{
                                        alignSelf: 'center',
                                        marginTop: 100
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            color: '#fff'
                                        }
                                    ]}>تعامل کنید و اعتماد بسازید</Text>
                            </Row>
                        </Grid>
                    </Content>
                </Image>
            </Container>
        );
    }
}
