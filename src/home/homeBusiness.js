import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab,
    Body
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import GiftedListView from 'react-native-gifted-listview';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    View,
    Dimensions,
    Animated,
    TouchableWithoutFeedback,
    Modal,
    Picker,
    ScrollView
} from 'react-native';
import Api from '../helper/api';
import SwipeALot from 'react-native-swipe-a-lot'

var styles = require('../style.js');
export default class HomeBusiness extends Component {
    constructor(props,
        context
    ) {
        super(props,
            context
        );
        this.state = {
            data: {
                slider: [],
                last: [],
                popular: [],
                random: []
            }
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({data: nextProps.data});
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Content theme={barghtheme} style={[
                        styles.mainWrapper, {
                            paddingTop: 50
                        }
                    ]}>
                    <View style={{
                            height: 250
                        }}>
                        <SwipeALot autoplay={{
                                enabled: true,
                                disableOnSwipe: true
                            }} circleDefaultStyle={{
                                marginTop: 20,
                                backgroundColor: '#2c3e50',
                                width: 10,
                                height: 10,
                                margin: 5
                            }}>
                            {
                                this
                                    .state
                                    .data
                                    .slider
                                    .map((s,
                                        i
                                    ) => {
                                        return <TouchableWithoutFeedback>
                                            <View style={styles.wrapper}>
                                                <Image resizeMode="stretch" source={{
                                                        uri: s.picture
                                                    }} style={[{
                                                            width: width,
                                                            height: 250
                                                        }
                                                    ]}/>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    })
                            }
                        </SwipeALot>
                    </View>
                    <Row style={[{
                                backgroundColor: '#fff'
                            }
                        ]}>
                        <ScrollView ref={(scrollView) => {
                                _scrollView = scrollView;
                            }} automaticallyAdjustContentInsets={true} scrollEventThrottle={200} horizontal={true} style={[
                                styles.scrollView, {
                                    paddingTop: 10,
                                    paddingBottom: 10
                                }
                            ]}>
                            {
                                this
                                    .state
                                    .data
                                    .random
                                    .map((s,
                                        i
                                    ) => {
                                        return <TouchableWithoutFeedback >
                                            <View style={[{
                                                        justifyContent: 'center',
                                                        alignItems: 'center'
                                                    }
                                                ]}>
                                                <Image source={{
                                                        uri: s.thumb.small
                                                    }} style={[{
                                                            width: 70,
                                                            height: 70,
                                                            borderRadius: 50,
                                                            borderColor: '#9650ec',
                                                            borderWidth: 2,
                                                            alignSelf: 'center'
                                                        }
                                                    ]}/>
                                                <Text style={[
                                                        styles.fontSans, {
                                                            color: '#000',
                                                            paddingTop: 7,
                                                            fontSize: 12
                                                        }
                                                    ]}>{s.name}</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    })
                            }

                        </ScrollView>
                    </Row>
                    <Text style={[styles.fontSans, styles.wrapperHeader]}>پربازدید ترین محصولات</Text>
                    <Row style={[{
                            paddingRight: 10,
                            paddingLeft: 10
                        }
                    ]}>
                    {
                        this
                            .state
                            .data
                            .popular
                            .map((s,
                                i
                            ) => {
                                return <View style={[{
                                                paddingRight: 5,
                                                width: width / 2
                                            }
                                        ]}>
                                        <Card>
                                            <CardItem>
                                                <Image source={require('../resource/images/cover1.jpeg')} style={[{
                                                            height: 100,
                                                            alignSelf: 'stretch'
                                                        }
                                                    ]}/>
                                                <View style={{
                                                        padding: 10
                                                    }}>
                                                    <Row style={[{
                                                                justifyContent: 'flex-end'
                                                            }
                                                        ]}>
                                                        <Text style={[
                                                                styles.fontSans, {
                                                                    fontSize: 13
                                                                }
                                                            ]}>
                                                            {s.name}
                                                        </Text>
                                                    </Row>
                                                    <Row style={{
                                                            marginTop: 3,
                                                        }}>
                                                        <Col>
                                                            <Row>
                                                                <Icon name="remove-red-eye" style={[{
                                                                    color:'#6d8093',
                                                                    fontSize: 15
                                                                }]}></Icon>
                                                                <Text style={[styles.fontSansLight,{
                                                                    fontSize:11,                                                                                                                 color:'#6d8093',
                                                                    paddingLeft:5
                                                            }]}>{s.view}</Text>
                                                            </Row>
                                                        </Col>
                                                        <Col>
                                                            <Text style={[
                                                                    styles.fontSans, {
                                                                        fontSize: 11
                                                                    }
                                                                ]}>
                                                                {s.price} ریال
                                                            </Text>
                                                        </Col>
                                                    </Row>
                                                </View>
                                            </CardItem>
                                        </Card>
                                    </View>
                            })
                    }
                    </Row>


                     <Text style={[styles.fontSans, styles.wrapperHeader, {
                         paddingTop: 10
                     }]}> آخرین محصولات</Text>
                    <Row style={[{
                            paddingRight: 10,
                            paddingLeft: 10
                        }
                    ]}>
                    {
                        this
                            .state
                            .data
                            .last
                            .map((s,
                                i
                            ) => {
                                return <View style={[{
                                                paddingRight: 5,
                                                width: width / 2
                                            }
                                        ]}>
                                        <Card>
                                            <CardItem>
                                                <Image source={require('../resource/images/cover1.jpeg')} style={[{
                                                            height: 100,
                                                            alignSelf: 'stretch'
                                                        }
                                                    ]}/>
                                                <View style={{
                                                        padding: 10
                                                    }}>
                                                    <Row style={[{
                                                                justifyContent: 'flex-end'
                                                            }
                                                        ]}>
                                                        <Text style={[
                                                                styles.fontSans, {
                                                                    fontSize: 13
                                                                }
                                                            ]}>
                                                            {s.name}
                                                        </Text>
                                                    </Row>
                                                    <Row style={{
                                                            marginTop: 3,
                                                        }}>
                                                        <Col>
                                                            <Row>
                                                                <Icon name="remove-red-eye" style={[{
                                                                    color:'#6d8093',
                                                                    fontSize: 15
                                                                }]}></Icon>
                                                                <Text style={[styles.fontSansLight,{
                                                                    fontSize:11,                                                                                                                 color:'#6d8093',
                                                                    paddingLeft:5
                                                            }]}>{s.view}</Text>
                                                            </Row>
                                                        </Col>
                                                        <Col>
                                                            <Text style={[
                                                                    styles.fontSans, {
                                                                        fontSize: 11
                                                                    }
                                                                ]}>
                                                                {s.price} ریال
                                                            </Text>
                                                        </Col>
                                                    </Row>
                                                </View>
                                            </CardItem>
                                        </Card>
                                    </View>
                            })
                    }
                    </Row>
                </Content>
            </Container>
        );
    }
}
