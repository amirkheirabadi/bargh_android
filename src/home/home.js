import React, {Component} from 'react';
import Drawer from 'react-native-drawer';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Menu from '../common/menu';
import Api from '../helper/api';
import Signin from '../auth/signin';
import HomePage from '../home/homePage';
import HomeCategory from '../home/homeCategory';
import HomeBusiness from '../home/homeBusiness';
import {
    Container,
    Content,
    Button,
    Grid,
    Row,
    Col,
    Header
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import Toast from 'react-native-root-toast';
import ScrollableTabView, {ScrollableTabBar} from 'react-native-scrollable-tab-view';
import {
    StyleSheet,
    Text,
    View,
    BackAndroid,
    Dimensions,
    Linking
} from 'react-native';
var styles = require('../style.js');

var homeTab;
export default class Home extends Component {
    constructor(props,
        context
    ) {
        super(props,
            context
        );
        this.state = {
            exitTimer: 0,
            user: '',
            drawerType: 'overlay',
            closedDrawerOffset: 0,
            panOpenMask: .1,
            panCloseMask: .5,
            relativeDrag: false,
            panThreshold: .25,
            tweenHandlerOn: false,
            tweenDuration: 200,
            tweenEasing: 'linear',
            disabled: false,
            tweenHandlerPreset: null,
            acceptDoubleTap: false,
            acceptTap: false,
            acceptPan: true,
            tapToClose: true,
            open: false,
            negotiatePan: false,
        };
    }
    async signout() {
        let status = await Api.userSignout();
        if (status) {
            this
                ._drawer
                .close();
            this.setState({user: ''});
        }
    }
    componentDidMount() {
        this
            .fetch()
            .done();

        BackAndroid.addEventListener('hardwareBackPress',
            () => {
            var time = (new Date()).getTime();
            if (time - this.state.exitTimer > 3000) {
                this.state.exitTimer = time;
                Toast.show('برای خروج از برنامه دوبار کلیک کنید .', {
                    duration: Toast.durations.SHORT,
                    position: -50,
                    backgroundColor: '#34495e',
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    textStyle: {
                        color: '#fff',
                        fontFamily: 'IRANSans_Light'
                    }
                }
                );
                return true;
            }
            BackAndroid.exitApp();
            return false;
        }
        );
    }

    async fetch() {
        let user = await Api.userFetch();
        if (user) {
            this.setState({user: JSON.parse(user)});
        }
        let response = await Api.request('home',
            'POST', {}
        );
        if (response) {
            homeTab.updateData(response[1]['data']['home']);
        }
    }

    openDrawer() {
        this
            ._drawer
            .open(true)
    }
    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Drawer ref={(ref) => this._drawer = ref} open={this.state.open} type={this.state.drawerType} animation={this.state.animation} openDrawerOffset={(viewport) => (viewport.width / 2) - 50} closedDrawerOffset={this.state.closedDrawerOffset} panOpenMask={this.state.panOpenMask} panCloseMask={this.state.panCloseMask} relativeDrag={this.state.relativeDrag} panThreshold={this.state.panThreshold} content={<Menu signout = {
                    this
                        .signout
                        .bind(this)
                } />} disabled={this.state.disabled} tweenHandle={(ratio) => ({
                    main: {
                        opacity: (2 - ratio) / 2
                    }
                })} tweenDuration={this.state.tweenDuration} tweenEasing={this.state.tweenEasing} acceptDoubleTap={this.state.acceptDoubleTap} acceptTap={this.state.acceptTap} acceptPan={this.state.acceptPan} tapToClose={this.state.tapToClose} negotiatePan={this.state.negotiatePan} changeVal={this.state.changeVal} side='right'>
                <Container>
                    <View style={styles.headerSection}>
                        <Grid>
                            <Col style={[
                                    styles.pullLeft, {
                                        paddingLeft: 10,
                                        paddingTop: 5
                                    }
                                ]}>
                                {
                                    this.state.user
                                        ? (
                                            <Button style={[{
                                                        paddingTop: -5
                                                    }
                                                ]} transparent="transparent">
                                                <Icon name='textsms' style={{
                                                        fontSize: 30,
                                                        color: '#fff'
                                                    }}/>
                                            </Button>
                                        )
                                        : (
                                            <View></View>
                                        )
                                }
                            </Col>
                            <Col style={[styles.pullRight]}>
                                <Text style={[
                                        styles.fontSans, {
                                            paddingRight: 15,
                                            color: '#fff',
                                            fontSize: 15
                                        }
                                    ]}>برق چین</Text>
                                <Button style={[{
                                            paddingTop: -5
                                        }
                                    ]} transparent="transparent" onPress={() => {
                                        this.openDrawer()
                                    }}>
                                    <Icon name='menu' style={{
                                            fontSize: 30,
                                            color: '#fff'
                                        }}/>
                                </Button>
                            </Col>
                        </Grid>
                    </View>
                    <Content>
                        <ScrollableTabView tabBarBackgroundColor="#5c4b8c" tabBarActiveTextColor="#fff" tabBarInactiveTextColor="#fff" page={0} tabBarPosition="overlayTop" tabBarTextStyle={{
                                fontFamily: 'IRANSans_Medium'
                            }} tabBarUnderlineStyle={{
                                backgroundColor: '#ecf0f1'
                            }} renderTabBar={() => <ScrollableTabBar/>} locked={true} style={[{
                                    height: height - 80
                                }
                            ]}>

                            <HomePage key="homePage" ref={(ref) => {
                                homeTab = ref
                            }} tabLabel="صفحه اصلی"/>

                        </ScrollableTabView>
                    </Content>
                </Container>
                <Button style={[{
                            backgroundColor: '#5c4b8c',
                            position: 'absolute',
                            shadowColor: '#2f4050',
                            left: 20,
                            shadowOpacity: 10,
                            shadowOffset: {
                                width: 30,
                                height: 30
                            },
                            shadowRadius: 50,
                            top: height - 100,
                            paddingTop: 5,
                            borderRadius: 50,
                            height: 50,
                            width: 50
                        }
                    ]}>
                    <Icon name='search' style={{
                            fontSize: 32,
                            color: '#fff'
                        }}/>
                </Button>
            </Drawer>
        );
    }
}