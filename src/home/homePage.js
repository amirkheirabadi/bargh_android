import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab,
    Body
} from 'native-base';

import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import GiftedListView from 'react-native-gifted-listview';
import {Actions} from 'react-native-router-flux';
import SliderModule from '../modules/sliderModule';
import ProductHorizonta from '../modules/productHorizontal';

import {
    Image,
    Text,
    View,
    Dimensions,
    Animated,
    TouchableWithoutFeedback,
    Modal,
    Picker,
    ScrollView,
    Linking
} from 'react-native';
import Api from '../helper/api';

var Styles = require('../style.js');
export default class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                slider: [
                    {}
                ],
                popular_product: [],
                last_product: []
            }
        };
    }

    updateData(data) {
        this.setState({data: data});
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Content theme={barghtheme} style={[Styles.mainWrapper]}>
                    <ScrollView style={[Styles.container]}>
                        <SliderModule data={this.state.data.slider}/>

                        <ProductHorizonta data={this.state.data.popular_product} title="پر بازدیدترین ترین محصولات هفته"/>
    
                        <Text style={[
                                Styles.fontSans,
                                Styles.wrapperHeader, {
                                    paddingTop: 10
                                }
                            ]}>
                            آخرین محصولات</Text>
                        <Row style={[{
                                    paddingRight: 10,
                                    paddingLeft: 10
                                }
                            ]}>
                            {
                                this
                                    .state
                                    .data
                                    .popular_product
                                    .map((s,
                                        i
                                    ) => {
                                        return <View style={[{
                                                    paddingRight: 5,
                                                    width: width / 2
                                                }
                                            ]}>
                                            <Card>
                                                <CardItem>
                                                    <Image source={require('../resource/images/cover1.jpeg')} style={[{
                                                                height: 100,
                                                                alignSelf: 'stretch'
                                                            }
                                                        ]}/>
                                                    <View style={{
                                                            padding: 10
                                                        }}>
                                                        <Row style={[{
                                                                    justifyContent: 'flex-end'
                                                                }
                                                            ]}>
                                                            <Text style={[
                                                                    Styles.fontSans, {
                                                                        fontSize: 13
                                                                    }
                                                                ]}>
                                                                {s.name}
                                                            </Text>
                                                        </Row>
                                                        <Row style={{
                                                                marginTop: 3
                                                            }}>
                                                            <Col>
                                                                <Row>
                                                                    <Icon name="remove-red-eye" style={[{
                                                                                color: '#6d8093',
                                                                                fontSize: 15
                                                                            }
                                                                        ]}></Icon>
                                                                    <Text style={[
                                                                            Styles.fontSansLight, {
                                                                                fontSize: 11,
                                                                                color: '#6d8093',
                                                                                paddingLeft: 5
                                                                            }
                                                                        ]}>
                                                                        {s.view}
                                                                    </Text>
                                                                </Row>
                                                            </Col>
                                                            <Col>
                                                                <Text style={[
                                                                        Styles.fontSans, {
                                                                            fontSize: 11
                                                                        }
                                                                    ]}>
                                                                    {s.price}
                                                                    ریال
                                                                </Text>
                                                            </Col>
                                                        </Row>
                                                    </View>
                                                </CardItem>
                                            </Card>
                                        </View>
                                    })
                            }
                        </Row>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}
