import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab,
    Body
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import GiftedListView from 'react-native-gifted-listview';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    View,
    Dimensions,
    Animated,
    TouchableWithoutFeedback,
    Modal,
    Picker,
    ScrollView
} from 'react-native';
import Api from '../helper/api';
import SwipeALot from 'react-native-swipe-a-lot'

var styles = require('../style.js');
export default class HomeCategory extends Component {
    constructor(props,
        context
    ) {
        super(props,
            context
        );
        this.state = {
            categories: [],
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({categories: nextProps.data.categories});
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Content theme={barghtheme} style={[
                        styles.mainWrapper, {
                            paddingTop: 50
                        }
                    ]}>
                   
                    <Row style={[{
                            paddingRight: 10,
                            paddingLeft: 10
                        }
                    ]}>
                    {
                        this.state.categories.map((s,
                                i
                            ) => {
                                return <View style={[{
                                                paddingRight: 5,
                                                width: width / 2
                                            }
                                        ]}>
                                        <Card>
                                            <CardItem >
                                                <Image source={require('../resource/images/cover1.jpeg')} style={[{
                                                            height: 200,
                                                            alignSelf: 'stretch',
                                                            
                                                        }
                                                    ]}>
                                                <View style={{
                                                        padding: 10,
                                                        position: 'absolute',
                                                        width: width / 2,
                                                        height: 200,
                                                        opacity: 0.5,
                                                        backgroundColor: 'black',
                                                    }}>
                                                </View>

                                                <View style={{position: 'absolute', left: 0,width: width / 2,top:165,justifyContent: 'center', alignItems: 'center'}}>
                                                    <Text style={[
                                                                styles.fontSans, {
                                                                    fontSize: 15,
                                                                    color: '#fff',
                                                                }
                                                            ]}>{s.name}</Text>
                                                </View>
                                                  
                                                </Image>
                                            </CardItem>
                                        </Card>
                                    </View>
                            })
                    }
                    </Row>

                </Content>
            </Container>
        );
    }
}
