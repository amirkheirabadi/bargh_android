import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem
} from 'native-base';
import barghtheme from '../theme/barghchin';
import GiftedListView from 'react-native-gifted-listview';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    View,
    Dimensions,
    Animated,
    TouchableHighlight,
    TouchableWithoutFeedback,
    ScrollView
} from 'react-native';
import Api from '../helper/api';

var Styles = require('../style.js');
const width = Dimensions
    .get('window')
    .width
const height = Dimensions
    .get('window')
    .height
export default class Articles extends Component {
    constructor(props) {
        super(props);
    }

    async _onFetch(page = 1,
        callback,
        options
    ) {

        let response = await Api.request('articles?page=' +
                page,
            'GET'
        );
        if (response[0] == 200) {
            return callback([response[1]['data']])
        }
    }

    _renderRowView(rowData) {
        return (
            <View style={[Styles.innerContainer]}>
                {
                    rowData.map((s,
                        i
                    ) => {
                        return <View>
                            <TouchableWithoutFeedback onPress={() => Actions.ArticleItem({id: s.id, title: s.title,
                            role: s.role})}>
                                <View style={[Styles.newsItem]}>
                                    <View style={[Styles.newIntro]}>
                                        <Text style={[Styles.fontSans, {paddingBottom: 10}]}>
                                            {s['title']}
                                        </Text>
                                        <Text style={[Styles.fontSansLight,{fontSize: 11,paddingBottom: 5}]}>
                                            نویسنده :  ( {s.role} ) {s.writter}
                                        </Text>
                                        <Text style={[Styles.fontSansLight,{fontSize: 11}]}>
                                            تاریخ ارسال : {s.created_at}
                                        </Text>
                                    </View>
                                    <Image resizeMode={"cover"} source={{
                                            uri: s['thumb']
                                        }} style={[Styles.newsCover]}/>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    })
                }
            </View>
        );
    }

    _renderPaginationWaitingView(paginateCallback) {
        return (
           <View style={{marginVertical: 30}}>
             <TouchableWithoutFeedback onPress={paginateCallback} >
                <View style={[Styles.loadMore]}>
                    <Text style={[Styles.fontSansLight, Styles.loadMoreText]}>
                        نمایش بیشتر
                    </Text>
                </View>
            </TouchableWithoutFeedback>
           </View>
        );
    }

    _emptyDisplay() {
        return (
            <View style={[{
                        marginTop: 10,
                        padding: 10,
                        alignSelf: 'center'
                    }
                ]}>
                <Text style={[
                        Styles.fontSans, {
                            textAlign: 'center'
                        }
                    ]}>
                    نتیجه ای برای نمایش وجود ندارد .
                </Text>
            </View>
        );
    }

    render() {
        return (
            <Container>
                    <Header style={[Styles.HeaderWrapper]}>
                        <Button onPress={() => {
                                Actions.pop()
                            }} iconLeft="iconLeft" transparent="transparent">
                            <Icon name='chevron-left' style={[Styles.headerActionIcon]}/>
                            <View>
                            <Text style={[Styles.fontSans,Styles.headerActionText]}>بازگشت</Text>
                            </View>
                        </Button>
                        <Title style={[Styles.fontSans, Styles.headerTitleRight]}>مقالات</Title>
                    </Header>
                    <Content theme={barghtheme} style={[Styles.mainWrapper]}>

                    <ScrollView style={[Styles.container]}>
                        <GiftedListView enableEmptySections={true} r="r" ef="listview" emptyView={this._emptyDisplay} rowView={this
                                ._renderRowView
                                .bind(this)} onFetch={this._onFetch} firstLoader={true} refreshable={false} pagination={true} withSections={false} customStyles={{
                                paginationView: {
                                    backgroundColor: '#eee'
                                }
                            }} refreshableTintColor="blue" paginationWaitingView={this._renderPaginationWaitingView}/>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}
