import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Picker,
    Item
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    Modal,
    View,
    TouchableWithoutFeedback,
    Dimensions
} from 'react-native';
import Api from '../helper/api';
var styles = require('../style.js');
export default class Importer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,

            name: '',
            owner: '',
            mobile: '',
            description: '',
            phone: '',
            website: '',
            province: '',
            provinces: [],
            city: '',
            cities: [],
            address: '',
            geo: '',
            brands: [],
            categories: [],
            user: "",

            catalog: "",
            catalogName: "",
            importer: '',
        };
    }

    componentWillReceiveProps(props) {
        if ("geo" in props) {
            this.setState({
                geo: props['geo']
            });
        }

        if ("brand" in props) {
            brands = this.state.brands;
            exists = false;
            brands.forEach(function (brand) {
                if (brand.name_en == props['brand']['name_en']) {
                    exists = true
                } 
            });
            if (!exists) {
                brands.push(props['brand']);
                this.setState({
                    brands :brands 
                });
            }
        }

        if ("category" in props) {
            categories = this.state.categories;
            exists = false;
            categories.forEach(function (brand) {
                if (brand.name == props['category']['name']) {
                    exists = true
                } 
            });
            if (!exists) {
                categories.push(props['category']);
                this.setState({
                    categories :categories 
                });
            }
        }
    }

    showModal(visible) {   
        this.setState({modalVisible: visible});
    }

    removeBrand(name) {
        brands = this.state.brands;
        brands.forEach(function (brand, index) {
            if (brand.name_en == name) {
                brands.splice(index, 1);
            } 
        });
        this.setState({
            brands: brands
        });
    }

    removeCategory(name) {
        categories = this.state.categories;
        categories.forEach(function (category, index) {
            if (brand.name_en == name) {
                categories.splice(index, 1);
            } 
        });
        this.setState({
            categories: categories
        });
    }

    chooseCatalog () {
        const FilePickerManager = require('NativeModules').FilePickerManager;
        FilePickerManager.showFilePicker(null, (response) => {
          if (!response.didCancel && !response.error) {
            var fileName = response.path;
            if (fileName.substr(fileName.lastIndexOf('.') + 1) != "pdf" && fileName.substr(fileName.lastIndexOf('.') + 1) != "PDF") {
                Toast.show('تنها فایل pdf به عنوان کاتالوگ مورد قبول می باشد .', {
                    duration: Toast.durations.LONG,
                    position: -50,
                    backgroundColor: '#e74c3c',
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    textStyle : {color : '#fff'}
                });
            } else {
                this.setState({
                    catalog: response.uri,
                    catalogName : fileName.substr(fileName.lastIndexOf('/') + 1)
                });
            }
          }
        });
    }

    async componentDidMount() {
        if (this.props.importer) {
            var importer = this.props.importer;
            this.setState({
                name: importer.name ,
                owner: importer.owner ,
                mobile: importer.mobile ,
                description: importer.description ,
                phone: importer.phone ,
                website: importer.website ,
                address: importer.address.address,
                brands: importer.brands,
                categories: importer.categories,
                geo: {
                    latitude: importer.address.lat,
                    longitude: importer.address.long
                }
            });
        }

        let provinces = await Api.request('provinces', 'POST');
        if (provinces[0] == 200) {
            this.setState({
                provinces: provinces[1]['data'],
            });
            if (this.props.importer) {
                this.setState({
                    province: this.props.importer.address.province_id,
                    city: this.props.importer.address.city
                });
                this.chooseProvince(this.state.province, this.state.city);       
            } else {
                this.setState({
                    province: provinces[1]['data'][0]['id'],
                });
                this.chooseProvince(this.state.provinces[0]['id']);                
            }
        }

        let user = await Api.userFetch();
        this.state.user = user;
    }

    async chooseProvince(value, city = null) {
        this.setState({province: value});
        let cities = await Api.request('cities', 'POST', {'province': value});
        if (cities[0] == 200) {
            if (city) {
                this.setState({
                    cities: cities[1]['data'],
                    city: city
                });
            } else {
                this.setState({
                    cities: cities[1]['data'],
                    city: cities[1]['data'][0]['id']
                });
            }
        }
    }

    check() {
        if (!this.state.geo || typeof this.state.geo != "object" || !"latitude" in this.state.geo || !"longitude" in this.state.geo) {
            Toast.show('لطفا موقعیت مکانی خود را بروی نقشه مشخص کنید .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#e74c3c',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle : {color : '#fff'}
            });
            return;
        }

        if (this.state.user.mobile == this.state.mobile) {
            return this.showModal(true);
        } else {
            return this.save();
        }
    }

    async save() {
        this.showModal(false);
        var files = {};
        if (this.state.catalog) {
            files = {
                'catalog' : {
                    'filename' :this.state.catalogName,
                    'data': this.state.catalog,
                    'type' : 'application/pdf'
                }
            }
        }
        let response = await Api.request('user/importer', 'POST', {
            'name' : this.state.name,
            'owner' : this.state.owner,
            'phone' : this.state.phone,
            'description' : this.state.description,
            'mobile' : this.state.mobile,
            'website' : this.state.website,
            'categories' : this.state.categories,
            'brands' : this.state.brands,
            'address_province' : this.state.province,
            'address_city' : this.state.city,
            'address' : this.state.address,
            'geo' : this.state.geo,
        },files);
        if (response[0] == 200) {
            Toast.show('اطلاعات کسب و کار شما با موفقیت در سیستم ثبت شد .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#2ecc71',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle : {color : '#fff'}
            });
            return Actions.Business();
        }
    }

     render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        return (
            <Container>
                <Content theme={barghtheme} style={styles.mainWrapper}>
                 <Modal styles={{
                        flex: 1,
                        backgroundColor: '#000'
                    }} animationType={"fade"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {
                        this.showModal(!this.state.modalVisible)
                    }}>
                        <Container style={styles.authModalWrapper}>
                            <View style={[styles.otherModal,{
                                    height: 200,
                                    padding: 20
                                }]}>
                                <Row>
                                    <Text style={[styles.fontSans,{
                                        textAlign:'justify',
                                    }]}>شماره موبایل وارد شده مشابه شماره موبایل حساب کاربری اصلی شما می باشد . از انجام این کار مطمئنید ؟</Text>
                                </Row>
                                   <Row style={[{
                                    marginTop:30
                                   }]}>
                                    <Button onPress={() => {
                                        this.save();
                                    }} transparent iconRight style={{
                                        padding: 0,
                                        margin: 0,
                                        width: 120
                                    }} textStyle={{
                                        fontFamily: 'IRANSans_Medium',
                                        marginRight: -30,
                                        fontSize: 11
                                    }}>
                                        <Text >تایید</Text>
                                        <Icon name='check'/>
                                    </Button>
                                    <Button onPress={() => {
                                        this.showModal(false)
                                    }} transparent iconRight style={{
                                        padding: 0,
                                        margin: 0,
                                        width: 120,
                                        marginLeft: 40
                                    }} textStyle={{
                                        fontFamily: 'IRANSans_Medium',
                                        marginRight: -30,
                                        fontSize: 11
                                    }}>
                                        انصراف
                                        <Icon name='close'/>
                                    </Button>
                                </Row>
                            </View>
                        </Container>
                    </Modal>
                    <Header style={[{
                            flexDirection: 'row'
                        }
                    ]}>
                        <Button onPress={() => {
                            this.check()
                        }} iconRight transparent>
                            <Icon name='done'/>
                            {this.props.company? (
                                <Text>ویرایش</Text>
                            ):(
                                <Text>ایجاد</Text>
                            )}
                        </Button>
                        <Title style={[
                            styles.fontSans, {
                                color: '#fff',
                                textAlign: 'center',
                                alignSelf: 'flex-end',
                                fontSize: 16
                            }
                        ]}>وارد کننده</Title>
                    </Header>
                    <Text style={[styles.fontSans, styles.wrapperHeader,{
                        marginTop: 5
                    }]}>اطلاعات اولیه</Text>
                    <Card style={[{
                            width: width * 0.97,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0
                            }
                        ]}>
                            <List style={[{
                                    padding: 0
                                }
                            ]}>
                                <ListItem style={{
                                    borderWidth: 0,
                                    paddingTop: 7,
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    paddingBottom: 5
                                }}>
                                    <InputGroup borderType='regular' style={[{
                                            borderRadius: 5,
                                            backgroundColor: '#F9FAFB'
                                        }
                                    ]}>
                                        <Input placeholder='نام شرکت' value={this.state.name} onChangeText={(text) => {
                                            this.setState({name: text});
                                        }}/>
                                    </InputGroup>
                                </ListItem>
                                <ListItem style={{
                                    borderWidth: 0,
                                    paddingTop: 7,
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    paddingBottom: 5
                                }}>
                                    <InputGroup borderType='regular' style={[{
                                            borderRadius: 5,
                                            backgroundColor: '#F9FAFB'
                                        }
                                    ]}>
                                        <Input placeholder='مدیرعامل شرکت' value={this.state.owner} onChangeText={(text) => {
                                            this.setState({owner: text});
                                        }}/>
                                    </InputGroup>
                                </ListItem>

                                <ListItem style={{
                                    borderWidth: 0,
                                    paddingTop: 7,
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                }}>
                                    <InputGroup borderType='regular' style={[{
                                            borderRadius: 5,
                                            backgroundColor: '#F9FAFB'
                                        }
                                    ]}>
                                        <Input placeholder='موبایل مدیرعامل' value={this.state.mobile} onChangeText={(text) => {
                                            this.setState({mobile: text});
                                        }}/>
                                    </InputGroup>
                                </ListItem>
                                <ListItem>
                                    <Text style={[styles.fontSans,{color: "#e67e22",fontSize: 12}]}>اطلاعات موبایل شما در برق چین برای کاربران دیگر نمایش داده نخواهد شد .</Text>
                                </ListItem>
                                <ListItem style={{
                                    borderWidth: 0,
                                    paddingTop: 7,
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    paddingBottom: 5
                                }}>
                                    <InputGroup borderType='regular' style={[{
                                            borderRadius: 5,
                                            backgroundColor: '#F9FAFB'
                                        }
                                    ]}>
                                        <Input style={[{height: 100}]} multiline={true}  value={this.state.description} placeholder='توضیحات' onChangeText={(text) => {
                                            this.setState({description: text});
                                        }}/>
                                    </InputGroup>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>

                    <Text style={[styles.fontSans, styles.wrapperHeader]}>راه ارتباطی</Text>
                    <Card style={[{
                            width: width * 0.97,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0
                            }
                        ]}>
                            <List style={[{
                                    padding: 0
                                }
                            ]}>

                                <ListItem style={{
                                    borderWidth: 0,
                                    paddingTop: 7,
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    paddingBottom: 5
                                }}>
                                    <InputGroup borderType='regular' style={[{
                                            borderRadius: 5,
                                            backgroundColor: '#F9FAFB'
                                        }
                                    ]}>
                                        <Input placeholder='شماره ثابت' value={this.state.phone} onChangeText={(text) => {
                                            this.setState({phone: text});
                                        }}/>
                                    </InputGroup>
                                </ListItem>

                                <ListItem style={{
                                    borderWidth: 0,
                                    paddingTop: 7,
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    paddingBottom: 5
                                }}>
                                    <InputGroup borderType='regular' style={[{
                                            borderRadius: 5,
                                            backgroundColor: '#F9FAFB'
                                        }
                                    ]}>
                                        <Input placeholder='وب سایت' value={this.state.website} onChangeText={(text) => {
                                            this.setState({website: text});
                                        }}/>
                                    </InputGroup>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>

                    <Text style={[styles.fontSans, styles.wrapperHeader]}>آدرس</Text>
                    <Card style={[{
                            width: width * 0.97,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0
                            }
                        ]}>
                            <List style={[{
                                    padding: 0
                                }
                            ]}>
                            <ListItem style={{
                                borderWidth: 0,
                                paddingTop: 7,
                                paddingRight: 10,
                                paddingLeft: 10,
                                paddingBottom: 5
                            }}>
                                <Col>
                                    <Text style={[styles.fontSansLight]}>انتخاب شهر</Text>
                                </Col>
                                <Col>
                                    <Text style={[styles.fontSansLight]}>انتخاب استان</Text>
                                </Col>
                            </ListItem>
                                <ListItem style={{
                                    borderWidth: 0
                                }}>
                                        <Col>
                                            <Picker mode="dropdown" selectedValue={this.state.city} onValueChange={(value) => {
                                                this.setState({city: value});
                                            }} style={[{
                                                    width: width * 0.50
                                                }
                                            ]}>
                                                {this.state.cities.map((s, i) => {
                                                    return <item value={s.id} label={s.name}/>
                                                })}
                                            </Picker>
                                        </Col>
                                        <Col style={[{
                                                paddingRight: 10
                                            }
                                        ]}>
                                            <Picker mode="dropdown" selectedValue={this.state.province} onValueChange={this.chooseProvince.bind(this)} style={[{
                                                    width: width * 0.50
                                                }
                                            ]}>
                                                {this.state.provinces.map((s, i) => {
                                                    return <item value={s.id} label={s.name}/>
                                                })}
                                            </Picker>
                                        </Col>
                                </ListItem>

                                <ListItem style={{
                                    borderWidth: 0,
                                    paddingTop: 7,
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    paddingBottom: 5
                                }}>
                                    <InputGroup borderType='regular' style={[{
                                            borderRadius: 5,
                                            backgroundColor: '#F9FAFB'
                                        }
                                    ]}>
                                        <Input style={[styles.fontSans, {
                                            height: 100,
                                            fontFamily: 'IRANSans_Medium'
                                        }]} multiline={true} value={this.state.address} placeholder='آدرس' onChangeText={(text) => {
                                            this.setState({address: text});
                                        }}/>
                                    </InputGroup>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>

                    <Card style={[{
                            width: width * 0.97,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0
                            }
                        ]}>
                        <TouchableWithoutFeedback onPress={() => {Actions.Map({
                            geo: this.state.geo
                        })}}>
                                <View style={[{flexDirection:'row',alignSelf: 'flex-end',padding:10}]}>
                                    <Text style={[styles.fontSans,{paddingTop:3}]}>انتخاب موقعیت بروی نقشه</Text>
                                    <Icon name="map" style={{
                                        color: '#bdc3c7',
                                        paddingLeft: 10
                                    }}/>
                                </View>
                            </TouchableWithoutFeedback>

                            </CardItem>
                     </Card>


                     <Card style={[{
                            width: width * 0.97,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0
                            }
                        ]}>
                        <TouchableWithoutFeedback onPress={() => {
                            this.chooseCatalog();
                        }}>
                                <View style={[{flexDirection:'row',alignSelf: 'flex-end',padding:10}]}>
                                    {this.state.catalogName ? (
                                        <Text style={[styles.fontSans,{paddingTop:3}]}>{this.state.catalogName}</Text>
                                    ) : (
                                        <Text style={[styles.fontSans,{paddingTop:3}]}>انتخاب کاتالوگ</Text>
                                    )}
                                    <Icon name="picture-as-pdf" style={{
                                        color: '#bdc3c7',
                                        paddingLeft: 10
                                    }}/>
                                </View>
                            </TouchableWithoutFeedback>

                            </CardItem>
                     </Card>



                    <Text style={[styles.fontSans, styles.wrapperHeader,{marginTop: 10}]}>برند ها واراداتی</Text>
                    <Card style={[{
                            width: width * 0.97,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0
                            }
                        ]}>
                            <List style={[{
                                    padding: 0
                                }
                            ]}>
                                {this.state.brands.map((s, i) => {
                                    return <ListItem style={{
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 3
                                    }}>
                                        <Col style={[{flexDirection:'row'}]}>
                                            <Button danger iconRight onPress={() => {
                                                this.removeBrand(s.name_en);
                                            }}>
                                                <Icon name='delete' style={[{fontSize:20}]}/>
                                                <Text style={[styles.fontSans,{fontSize:5}]}>حذف</Text>
                                            </Button>
                                        </Col>
                                        <Col>
                                            <View>
                                                <Text style={[styles.fontSans,{paddingRight:15,paddingTop:5,}]}>{s.name_fa}</Text>
                                            </View>
                                        </Col>
                                    </ListItem>
                                })}


                                <ListItem style={[{
                                        width: width * 0.97,
                                        padding:0,
                                        backgroundColor: '#8e44ad'
                                    }
                                ]}>
                                <Button full block iconRight
                                    onPress={() => {
                                        Actions.Brands();
                                    }}
                                    style={[{
                                        backgroundColor: '#8e44ad',
                                        alignSelf: 'center',
                                        height: 40,
                                        width: width * 0.97,
                                    }
                                ]}>
                                    <Icon name='add' style={[{
                                        color: '#fff',
                                    }]}/>
                                    <Text style={[
                                        styles.fontSans, {
                                            fontSize: 13,
                                            color: '#fff'
                                        }
                                    ]}>افزودن برند جدید</Text>
                                    </Button>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>


                    <Text style={[styles.fontSans, styles.wrapperHeader,{marginTop: 10}]}>نوع محصولات واراداتی</Text>
                    <Card style={[{
                            width: width * 0.97,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0
                            }
                        ]}>
                            <List style={[{
                                    padding: 0
                                }
                            ]}>
                                {this.state.categories.map((s, i) => {
                                    return <ListItem style={{
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 3
                                    }}>
                                        <Col style={[{flexDirection:'row'}]}>
                                            <Button danger iconRight onPress={() => {
                                                this.removeCategory(s.name);
                                            }}>
                                                <Icon name='delete' style={[{fontSize:20}]}/>
                                                <Text style={[styles.fontSans,{fontSize:5}]}>حذف</Text>
                                            </Button>
                                        </Col>
                                        <Col>
                                            <View>
                                                <Text style={[styles.fontSans,{paddingRight:15,paddingTop:5,}]}>{s.name}</Text>
                                            </View>
                                        </Col>
                                    </ListItem>
                                })}


                                <ListItem style={[{
                                        width: width * 0.97,
                                        padding:0,
                                        backgroundColor: '#8e44ad'
                                    }
                                ]}>
                                <Button full block iconRight
                                    onPress={() => {
                                        Actions.Categories({
                                            'useCountry': true
                                        });
                                    }}
                                    style={[{
                                        backgroundColor: '#8e44ad',
                                        alignSelf: 'center',
                                        height: 40,
                                        width: width * 0.97,
                                    }
                                ]}>
                                    <Icon name='add' style={[{
                                        color: '#fff',
                                    }]}/>
                                    <Text style={[
                                        styles.fontSans, {
                                            fontSize: 13,
                                            color: '#fff'
                                        }
                                    ]}>افزودن نوع محصول جدید</Text>
                                    </Button>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>


                </Content>
            </Container>
        );
    }
}
