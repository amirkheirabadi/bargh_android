import React, { Component } from 'react';
import {Actions} from 'react-native-router-flux';
import {Container, Content, InputGroup, Input, Icon, List, ListItem, Picker, Item, Button,Grid,Row} from 'native-base';
import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';

import {
    Text,
    Image,
} from 'react-native';
var styles = require('../style.js');
export default class Loading extends Component{
    render() {
        return (
            <Container style={[{backgroundColor: '#2C333D',}]}>
                <Image source={require('../resource/images/bg_main.png')} style={[{
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}>
                <Content>
                    <Grid>
                        <Row style={{alignSelf: 'center',marginTop:150}}>
                            <Image source={require('../resource/images/logo_main.png')} style={{width:150,height:150}}/>
                        </Row>
                        <Row style={{alignSelf: 'center',marginTop:80}}>
                             <Bars size={20} color="#FF4C74" />
                        </Row>
                        <Row style={[{alignSelf: 'center',marginTop:100}]}>
                            <Text style={[styles.fontSans,{color:'#fff'}]}>در حال بارگذاری ...</Text>
                        </Row>
                    </Grid>
                </Content>
                </Image>
            </Container>
        );
    }
}
