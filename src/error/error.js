import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import {
    Container,
    Content,
    Icon,
    Button,
    Grid,
    Row
} from 'native-base';

import barghtheme from '../theme/barghchin';
import {Text, NetInfo, BackAndroid, Image} from 'react-native';
var styles = require('../style.js');
export default class Error extends Component {
    componentWillMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
            BackAndroid.exitApp();
            return true
        }
        );
    }

    render() {
        return (
            <Container style={[{
                        backgroundColor: '#fff'
                    }
                ]}>
                <Image source={require('../resource/images/bg_main.png')} style={[{
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}>
                    <Content theme={barghtheme}>
                        <Grid>
                            <Row style={[{
                                        alignSelf: 'center',
                                        paddingTop: 200
                                    }
                                ]}>
                                <Icon name="error" style={{
                                        fontSize: 100,
                                        color: '#FF4C74'
                                    }}/>
                            </Row>
                            <Row style={[{
                                        alignSelf: 'center',
                                        marginTop: 50
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            'color': '#fff'
                                        }
                                    ]}>متاسفانه خطایی در برنامه به وجود آمده است .</Text>
                            </Row>
                             <Row style={[{
                                        alignSelf: 'center',
                                        marginTop: 20
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            'color': '#fff'
                                        }
                                    ]}>با پشتیبانی سسیستم تماس بگیرید .</Text>
                            </Row>
                            <Row style={[{
                                        alignSelf: 'center',
                                        marginTop: 60
                                    }
                                ]}>
                                <Button onPress={() => {BackAndroid.exitApp()}} info="info">
                                    <Text style={[
                                            styles.fontSans, {
                                                color: '#fff'
                                            }
                                        ]}>
                                        خروج از برنامه</Text>
                                </Button>
                            </Row>
                        </Grid>
                    </Content>
                </Image>
            </Container>

        );
    }
}
