import React, {Component} from 'react';
import {
    Container,
    Grid,
    Row,
    Content,
    Icon,
    Header,
    Title,
    Button,
    Fab
} from 'native-base';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import {Text, Dimensions, WebView, View, BackAndroid} from 'react-native';
import Share from 'react-native-share';
var config = require('../config.js');
var Styles = require('../style.js');
export default class About extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }
    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
                Actions.pop();
                return true;
            }
        );
    }

    onCancel() {
        this.setState({visible: false});
    }
    onOpen() {
        this.setState({visible: true});
    }

    onNavigationStateChange(event) {
        if (event.title) {
            const htmlHeight = Number(event.title) //convert to number
            this.setState({Height:htmlHeight});
        }
    }

    render() {
        let shareOptions = {
            title: "معرفی به دوستان",
            message: "برق چین بزرگترین سایت الکتریکی کشور . دانلود از :",
            subject: "معرفی به دوستان" //  for email
        };

        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Header style={[Styles.HeaderWrapper]}>
                    <Button onPress={() => {
                            Actions.pop()
                        }} iconLeft="iconLeft" transparent="transparent">
                        <Icon name='chevron-left' style={[Styles.headerActionIcon]}/>
                        <View>
                            <Text style={[Styles.fontSans, Styles.headerActionText]}>بازگشت</Text>
                        </View>
                    </Button>
                    <Title style={[Styles.fontSans, Styles.headerTitleRight]}>درباره ما</Title>
                </Header>
                <Content theme={barghtheme}>

                    <WebView 
                        scrollEnabled={false}
                        javaScriptEnabled ={true}
                        scrollEnabled={false}
                        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                        style={[{
                        flex: 1,
                        width: width,
                        height:this.state.Height
                            }
                        ]} 
                        source={{
                            uri: config.server +
                                    '/api/page/aboutus'
                        }}
                    />

                </Content>
                <Fab direction="right" containerStyle={{
                        marginLeft: 10
                    }} style={{
                        backgroundColor: '#5c4b8c'
                    }} position="bottomLeft" onPress={() => Share.open(shareOptions)}>
                    <Icon name="share"/>
                </Fab>
            </Container>
        );
    }
}
