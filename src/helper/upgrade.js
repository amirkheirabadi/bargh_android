import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import {
    Container,
    Content,
    Icon,
    Button,
    Grid,
    Row
} from 'native-base';

import barghtheme from '../theme/barghchin';
import {Text, BackAndroid, Image,Linking} from 'react-native';
var styles = require('../style.js');
export default class Upgrade extends Component {

    handleClick = () => {
        Linking.canOpenURL(this.props.url).then(supported => {
        if (supported) {
            Linking.openURL(this.props.url);
        } else {
            console.log('Don\'t know how to open URI: ' + this.props.url);
        }
        });
    };


    componentWillMount() {
        BackAndroid.addEventListener('hardwareBackPress',
                () => {
                return true
            }
        );
    }

    render() {
        return (
            <Container style={[{
                        backgroundColor: '#fff'
                    }
                ]}>
                <Image source={require('../resource/images/bg_main.png')} style={[{
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}>
                    <Content theme={barghtheme}>
                        <Grid>
                            <Row style={[{
                                        alignSelf: 'center',
                                        paddingTop: 200
                                    }
                                ]}>
                                <Icon name="update" style={{
                                        fontSize: 100,
                                        color: '#FF4C74'
                                    }}/>
                            </Row>
                            <Row style={[{
                                        alignSelf: 'center',
                                        marginTop: 50
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            'color': '#fff'
                                        }
                                    ]}>نسخه جدید برق جین منتشر شده است .</Text>
                            </Row>
                             <Row style={[{
                                        alignSelf: 'center',
                                        marginTop: 20
                                    }
                                ]}>
                                <Text style={[
                                        styles.fontSans, {
                                            'color': '#fff'
                                        }
                                    ]}>برای ادامه عملیات لطفا نسخه جدید را نصب کنید .</Text>
                            </Row>
                            <Row style={[{
                                        alignSelf: 'center',
                                        marginTop: 60
                                    }
                                ]}>
                                <Button onPress={() => {this.handleClick()}} info="info">
                                    <Text style={[
                                            styles.fontSans, {
                                                color: '#fff'
                                            }
                                        ]}>
                                        دریافت نسخه جدید</Text>
                                </Button>
                            </Row>
                        </Grid>
                    </Content>
                </Image>
            </Container>

        );
    }
}
