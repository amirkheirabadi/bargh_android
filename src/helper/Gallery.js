import React, {Component} from 'react';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import {Image, Text, View, Dimensions, Animated, TouchableWithoutFeedback,Modal} from 'react-native';
import PhotoBrowser from 'react-native-photo-browser';
var styles = require('../style.js');

export default class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            media: [],
            enableGrid: true,
            displayNavArrows: true,
            displayActionButton: false,
            initialIndex: 0,
            displaySelectionButtons: false,
            startOnGrid:false,
        };
    }

    componentDidMount() {

        console.log(this.props.media);
        // if (!this.props.media) {
        //     Actions.pop();
        // }

        this.setState({
            media: this.props.media
        });
    }

   
  
    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        return (
            <PhotoBrowser
            onBack={() => {Actions.pop()}}
            mediaList={this.state.media}
            initialIndex={this.state.initialIndex}
            displayNavArrows={this.state.displayNavArrows}
            displaySelectionButtons={this.state.displaySelectionButtons}
            displayActionButton={this.state.displayActionButton}
            startOnGrid={this.state.startOnGrid}
            enableGrid={this.state.enableGrid}
            useCircleProgress
          />
        );
    }
}
