import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab,
    
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import GiftedListView from 'react-native-gifted-listview';
import {Actions} from 'react-native-router-flux';
import {Image, Text, View, Dimensions, Animated, TouchableWithoutFeedback,Modal} from 'react-native';
import Api from '../helper/api';

var styles = require('../style.js');
export default class Brands extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name : '',
            modalVisible: false,
            new: 'yes',
        };
    }

    componentDidMount() {
        if (this.props.new) {
            this.setState({
               new: this.props.new 
            });
        }
    }

    async _onFetch(name, page = 1, callback, options) {
        let response = await Api.request('brands', 'POST', {
            name: name,
        });

        if (response[0] == 200) {
            return callback([response[1]['data']])
        }
    }

    _renderRowView(rowData) {
        return (
            <View>
                {rowData.map((s, i) => {
                return <ListItem>
                    <TouchableWithoutFeedback  onPress={() => this._onPress(s)}>
                       <View>
                         <Text>{s.name_fa}</Text>
                       </View>
                    </TouchableWithoutFeedback>
                    </ListItem>
                })}
            </View>
        );
    }

    _onPress(rowData) {
        Actions.pop({
          refresh: {brand: rowData}
        });
     }

    _refresh() {
        this.refs.listview._refresh();
    }

    _emptyDisplay(){
        return (
            <View style={[{
                    marginTop: 10,
                    padding:10,
                    alignSelf: 'center',
                }
            ]}>
            <Text style={[styles.fontSans,{textAlign:'center'}]}>
              نتیجه ای برای نمایش وجود ندارد .
            </Text>
            <Text style={[styles.fontSans,{textAlign:'center',marginTop:15,}]}>
                اگر برندی مد نظر شماست که در لیست برق چین وجود ندارد لطفا از طریق دکمه زیر آن را به ما اطلاع دهید .
            </Text>
           <Image resizeMode="stretch" source={require('../resource/images/arrow.png')} style={[{
                    marginTop:30,
                    width: 80,
                    height:120,
                    alignSelf: 'center'
                }
            ]}/>
          </View>
        );
    }
       
    new() {
        name_fa = this.state.name_fa;
        name_en = this.state.name_en;

        if (!name_fa || !name_en) {
            Toast.show('لطفا نام فارسی و انگلیسی برند مورد نظر خود را وارد کنید .', {
              duration: Toast.durations.LONG,
              position: -50,
              backgroundColor: '#e74c3c',
              shadow: true,
              animation: true,
              hideOnPress: true,
              delay: 0,
              textStyle : {color : '#fff'}
            });
            return ;
        }
        Actions.pop({
          refresh: {brand: {
            name_fa: name_fa,
            name_en: name_en
          }}
        });
    }
        
    showModal(visible) {
        this.setState({
            name_fa: '',
            name_en: ''
        });      
        this.setState({modalVisible: visible});
    }

    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        return (
            <Container>
                
                <Content theme={barghtheme}>
                <Modal styles={{
                        flex: 1,
                        backgroundColor: '#000'
                    }} animationType={"fade"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {
                        this.showModal(!this.state.modalVisible)
                    }}>
                        <Container style={styles.authModalWrapper}>
                            <View style={[styles.otherModal,{
                                    height: 200
                                }]}>
                                <Row>
                                    <InputGroup style={[{width:280}]}>
                                        <Input regular placeholder="نام فارسی"  onChangeText={(text) => {
                                            this.setState({
                                                name_fa : text
                                            });
                                        }}/>
                                    </InputGroup>
                                </Row>
                                <Row>
                                    <InputGroup style={[{width:280}]}>
                                        <Input regular placeholder="نام انگلیسی"  onChangeText={(text) => {
                                            this.setState({
                                                name_en : text
                                            });
                                        }}/>
                                    </InputGroup>
                                </Row>
                                   <Row style={[{
                                    marginTop:30
                                   }]}>
                                    <Button onPress={() => {
                                        this.new();
                                    }} transparent iconRight style={{
                                        padding: 0,
                                        margin: 0,
                                        width: 120
                                    }} textStyle={{
                                        fontFamily: 'IRANSans_Medium',
                                        marginRight: -30,
                                        fontSize: 11
                                    }}>
                                        <Text >تایید</Text>
                                        <Icon name='check'/>
                                    </Button>
                                    <Button onPress={() => {
                                        this.showModal(false)
                                    }} transparent iconRight style={{
                                        padding: 0,
                                        margin: 0,
                                        width: 120,
                                        marginLeft: 40
                                    }} textStyle={{
                                        fontFamily: 'IRANSans_Medium',
                                        marginRight: -30,
                                        fontSize: 11
                                    }}>
                                        انصراف
                                        <Icon name='close'/>
                                    </Button>
                                </Row>
                            </View>
                        </Container>
                    </Modal>
                    <Header style={[{
                            flexDirection: 'row'
                        }
                    ]}>
                        <Button onPress={() => {
                            Actions.pop()
                        }} iconLeft transparent>
                            <Icon name='chevron-left'/>
                            <Text>بازگشت</Text>
                        </Button>
                        <Title style={[
                            styles.fontSans, {
                                color: '#fff',
                                textAlign: 'center',
                                alignSelf: 'flex-end',
                                fontSize: 16
                            }
                        ]}>انتخاب برند</Title>
                    </Header>

                    <View style={[{
                            marginTop: 10,
                            width: width * 0.97,
                            alignSelf: 'center',
                        }
                    ]}>
                        <InputGroup>
                            <Button transparent onPress={() => {this._refresh()}}>
                                <Icon name='search' />
                            </Button>
                            <Input placeholder="جستجوی نام برند ..." onChangeText={(text) => {
                                this.setState({name: text});
                            }}/>
                        </InputGroup>
                    </View>
                    <Text style={[
                        styles.fontSans,styles.wrapperHeader, {
                            marginTop: 20,
                        }]}>نتایج جستجو :</Text>

                        <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center',
                                backgroundColor: 'rgb(245, 245, 245)'
                            }
                        ]}>
                            <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                                <List style={[{
                                        padding: 0
                                    }
                                ]}>

                    <View style={styles.container}>
                        <View style={styles.navBar}/>
                        <GiftedListView enableEmptySections={true} 
                        ref="listview" emptyView={this._emptyDisplay} 
                        rowView={this._renderRowView.bind(this)} 
                        onFetch={this._onFetch.bind(this, this.state.name)} 
                        firstLoader={true} // display a loader for the first fetching
                            pagination={false} // enable infinite scrolling using touch to load more
                            withSections={false} // enable sections
                            customStyles={{
                            paginationView: {
                                backgroundColor: '#eee'
                            }
                        }} refreshableTintColor="blue"/>
                    </View>

                        </List>
                    </CardItem>
                </Card>
                </Content>
                {this.state.new == "yes"? (
                     <Fab
                        active={true}
                        direction="up"
                        style={{ backgroundColor: '#5c4b8c' }}
                        position="bottomLeft"
                        onPress={() => {this.showModal(true)}}
                    >
                        <Icon name="add-box" />
                    </Fab>
                ): (
                    <View></View>
                )}
            </Container>
        );
    }
}
