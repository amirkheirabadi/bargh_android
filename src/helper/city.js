import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem
} from 'native-base';
import barghtheme from '../theme/barghchin';
import GiftedListView from 'react-native-gifted-listview';
import {Actions} from 'react-native-router-flux';
import {Image, Text, View, Dimensions, Animated, TouchableWithoutFeedback} from 'react-native';
import Api from '../helper/api';

var styles = require('../style.js');
export default class City extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name : '',
            province: '',
        };
    }

    async componentDidMount() {
        let provinces = await Api.request('province', 'POST');
        if (province[0] == 200) {

        }
    }

    async _onFetch(name, page = 1, callback, options) {
        let response = await Api.request('brands', 'POST', {
            name: name,
        });
        if (response[0] == 200) {
            return callback(response[1]['data'])
        }
    }

    _renderRowView(rowData) {
        return (
            <ListItem>
                <TouchableWithoutFeedback  onPress={() => this._onPress(rowData)}>
                    <Text>{rowData.name_fa}</Text>
                </TouchableWithoutFeedback>
            </ListItem>
        );
    }

    _onPress(rowData) {
        console.log('clcik');
        Actions.pop({brand: rowData});
     }

    _refresh() {
        this.refs.listview._refresh();
    }

    _emptyDisplay(){
        return (
            <View style={[{
                    marginTop: 10,
                    padding:10,
                    alignSelf: 'center',
                }
            ]}>
            <Text style={[styles.fontSans]}>
              نتیجه ای برای نمایش وجود ندارد .
            </Text>
          </View>
        );
    }

    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        return (
            <Container>
                <Content theme={barghtheme}>
                    <Header style={[{
                            flexDirection: 'row'
                        }
                    ]}>
                        <Button onPress={() => {
                            Actions.pop()
                        }} iconLeft transparent>
                            <Icon name='chevron-left'/>
                            <Text>تایید</Text>
                        </Button>
                        <Title style={[
                            styles.fontSans, {
                                color: '#fff',
                                textAlign: 'center',
                                alignSelf: 'flex-end',
                                fontSize: 16
                            }
                        ]}>انتخاب شهر</Title>
                    </Header>

                    <View style={[{
                            marginTop: 10,
                            width: width * 0.97,
                            alignSelf: 'center',
                        }
                    ]}>

                    </View>
                    <Text style={[
                        styles.fontSans,styles.wrapperHeader, {
                            marginTop: 20,
                        }]}>نتایج جستجو :</Text>

                        <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center',
                                backgroundColor: 'rgb(245, 245, 245)'
                            }
                        ]}>
                            <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                                <List style={[{
                                        padding: 0
                                    }
                                ]}>

                    <View style={styles.container}>
                        <View style={styles.navBar}/>
                        <GiftedListView ref="listview" emptyView={this._emptyDisplay} rowView={this._renderRowView.bind(this)} onFetch={this._onFetch.bind(this, this.state.name)} firstLoader={true} // display a loader for the first fetching
                            pagination={false} // enable infinite scrolling using touch to load more
                            withSections={false} // enable sections
                            customStyles={{
                            paginationView: {
                                backgroundColor: '#eee'
                            }
                        }} refreshableTintColor="blue"/>
                    </View>

                        </List>
                    </CardItem>
                </Card>
                </Content>
            </Container>
        );
    }
}
