import React, { Component } from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab,
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import GiftedListView from 'react-native-gifted-listview';
import { Actions } from 'react-native-router-flux';
import { Image, Text, View, Dimensions, Animated, TouchableWithoutFeedback, Modal, Picker } from 'react-native';
import Api from '../helper/api';

var styles = require('../style.js');
export default class Categories extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countries: [],
            country: '',
            useCountry: false,

            name: '',
            newName: '',
            modalVisible: false
        };
    }

    async componentDidMount() {
        if (this.props.useCountry) {
            this.setState({
                useCountry: this.props.useCountry,
            });
        }
        let response = await Api.request('countries', 'GET');
        if (response[0] == 200) {
            console.log(response[0]['data']);
            this.setState({
                countries: response[1]['data'],
                country: response[1]['data'][0]['id']
            });
        }
    }

    async _onFetch(name, page = 1, callback, options) {
        let response = await Api.request('categories', 'POST', {
            name: name,
        });
        console.log(response);
        if (response[0] == 200) {
            return callback([response[1]['data']])
        }
    }

    choose() {
        if (!this.state.country || !this.state.name) {
            Toast.show('لطفا کشور و دسته بندی مورد نظر خود را انتخاب کنید .', {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: '#e74c3c',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle: { color: '#fff' }
            });
            return;
        }

        Actions.pop({
            refresh: {
                category: {
                    name: this.state.name,
                    country: this.state.country
                }
            }
        });
    }

    _renderRowView(rowData) {
        return (
            <View>
                {rowData.map((s, i) => {
                return <ListItem>
                    <TouchableWithoutFeedback  onPress={() => this._onPress(s)}>
                       <View>
                         <Text>{s.name}</Text>
                       </View>
                    </TouchableWithoutFeedback>
                    </ListItem>
                })}
            </View>
        );
    }

    _onPress(rowData) {
        this.setState({
            name: rowData.name
        });
    }

    _refresh() {
        this.refs.listview._refresh();
    }

    _emptyDisplay() {
        return ( <View style = {
                [{
                    marginTop: 10,
                    padding: 10,
                    alignSelf: 'center',
                }]
            } >
            <Text style={[styles.fontSans,{textAlign:'center'}]}>
              نتیجه ای برای نمایش وجود ندارد .
            </Text> 
            <Text style = {
                [styles.fontSans, { textAlign: 'center', marginTop: 15, }] } >
            اگر دسته بندی مد نظر شماست که در لیست برق چین وجود ندارد لطفا از طریق دکمه زیر آن را به ما اطلاع دهید. 
            </Text> 
            <Image resizeMode = "stretch"
            source = { require('../resource/images/arrow.png') }
            style = {
                [{
                    marginTop: 30,
                    width: 80,
                    height: 120,
                    alignSelf: 'center'
                }]
            }
            /> </View>
        );
    }

    new() {
        this.setState({
            name: this.state.newName
        });
        this.showModal(false);
    }

    showModal(visible) {
        if (true) {
            this.setState({
                newName: ''
            });
        }
        this.setState({ modalVisible: visible });
    }

    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        return (
            <Container>
                
                <Content theme={barghtheme}>
                <Modal styles={{
                        flex: 1,
                        backgroundColor: '#000'
                    }} animationType={"fade"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {
                        this.showModal(!this.state.modalVisible)
                    }}>
                        <Container style={styles.authModalWrapper}>
                            <View style={[styles.otherModal,{
                                    height: 150
                                }]}>
                                <Row>
                                    <InputGroup style={[{width:280}]}>
                                        <Input regular placeholder="نام دسته بندی"  onChangeText={(text) => {
                                            this.setState({
                                                newName : text
                                            });
                                        }}/>
                                    </InputGroup>
                                </Row>
                                   <Row style={[{
                                    marginTop:30
                                   }]}>
                                    <Button onPress={() => {
                                        this.new();
                                    }} transparent iconRight style={{
                                        padding: 0,
                                        margin: 0,
                                        width: 120
                                    }} textStyle={{
                                        fontFamily: 'IRANSans_Medium',
                                        marginRight: -30,
                                        fontSize: 11
                                    }}>
                                        <Text >تایید</Text>
                                        <Icon name='check'/>
                                    </Button>
                                    <Button onPress={() => {
                                        this.showModal(false)
                                    }} transparent iconRight style={{
                                        padding: 0,
                                        margin: 0,
                                        width: 120,
                                        marginLeft: 40
                                    }} textStyle={{
                                        fontFamily: 'IRANSans_Medium',
                                        marginRight: -30,
                                        fontSize: 11
                                    }}>
                                        انصراف
                                        <Icon name='close'/>
                                    </Button>
                                </Row>
                            </View>
                        </Container>
                    </Modal>
                    <Header style={[{
                            flexDirection: 'row'
                        }
                    ]}>
                        <Button onPress={() => {
                            this.choose()
                        }} iconLeft transparent>
                            <Icon name='chevron-left'/>
                            <Text style={[styles.fontSans]}>انتخاب</Text>
                        </Button>
                        <Title style={[
                            styles.fontSans, {
                                color: '#fff',
                                textAlign: 'center',
                                alignSelf: 'flex-end',
                                fontSize: 16
                            }
                        ]}>انتخاب دسته بندی</Title>
                    </Header>

                    <View style={[{
                            marginTop: 10,
                            width: width * 0.97,
                            alignSelf: 'center',
                        }
                    ]}>

                            {
                                this.state.useCountry ? (
                                    <Text style={[styles.fontSans,{textAlign:'right',marginTop:0,padding:5}]}>
                                        ابتدا با استفاده از باکس جستجو دسته بندی مورد نظر خود را پیدا کنید و سپس کشور وارد کننده خود را انتخاب کنید .
                                    </Text>
                                ): (
                                    <Text></Text>
                                )
                            }

                            <Row>
                                {
                                    this.state.useCountry? (
                                        <Col >
                                            <Picker mode="dropdown" selectedValue={this.state.country} onValueChange={(value) => {
                                                this.setState({country: value});
                                            }} style={[{
                                                    paddingRight:15,
                                                    paddingTop:30,
                                                }
                                            ]}>
                                                {this.state.countries.map((s, i) => {
                                                    return <item value={s.id} label={s.name}/>
                                                })}
                                            </Picker>
                                        </Col>
                                    ):(
                                        <Text></Text>
                                    )
                                }

                                <Col >
                                   <Row style={[{paddingTop:15,paddingRight:10,justifyContent: 'flex-end', alignSelf: 'flex-end'}]}>
                                    {this.state.name? (
                                            <Text style={[styles.fontSansLight,{paddingRight:10}]}>{this.state.name}</Text>
                                        ):(
                                            <Text style={[styles.fontSansLight,{paddingRight:10}]}>انتخاب نشده</Text>
                                        )}
                                    <Text style={[styles.fontSans]}>نام دسته بندی : </Text>
                                   </Row>
                                </Col>
                            </Row>

                            <InputGroup style={[{marginTop:10}]}>
                                <Button transparent onPress={() => {this._refresh()}}>
                                    <Icon name='search' />
                                </Button>
                                <Input placeholder="جستجوی نام دسته بندی ..." onChangeText={(text) => {
                                    this.setState({name: text});
                                }}/>
                            </InputGroup>



                    </View>
                    <Text style={[
                        styles.fontSans,styles.wrapperHeader, {
                            marginTop: 20,
                        }]}>نتایج جستجو :</Text>

                        <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center',
                                backgroundColor: 'rgb(245, 245, 245)'
                            }
                        ]}>
                            <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                                <List style={[{
                                        padding: 0
                                    }
                                ]}>

                    <View style={styles.container}>
                        <View style={styles.navBar}/>
                        <GiftedListView enableEmptySections={true} ref="listview" emptyView={this._emptyDisplay} 
                        rowView={this._renderRowView.bind(this)} 
                        onFetch={this._onFetch.bind(this, this.state.name)} 
                        firstLoader={true} // display a loader for the first fetching
                            pagination={false} // enable infinite scrolling using touch to load more
                            withSections={false} // enable sections
                            customStyles={{
                            paginationView: {
                                backgroundColor: '#eee'
                            }
                        }} refreshableTintColor="blue"/>
                    </View>

                        </List>
                    </CardItem>
                </Card>
                </Content>
                 <Fab
                        active={true}
                        direction="up"
                        style={{ backgroundColor: '#5c4b8c' }}
                        position="bottomLeft"
                        onPress={() => {this.showModal(true)}}
                    >
                        <Icon name="add-box" />
                    </Fab>
            </Container>
        );
    }
}
