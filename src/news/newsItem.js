import React, {Component} from 'react';
import {
    Container,
    Grid,
    Row,
    Col,
    Content,
    Icon,
    Header,
    Title,
    Button,
    Fab
} from 'native-base';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import {
    Text,
    Dimensions,
    View,
    ScrollView,
    Linking,
    Image,
    WebView
} from 'react-native';
import Share from 'react-native-share';
import Loading from '../Loading';
import Api from '../helper/api';
var config = require('../config.js');
var Styles = require('../style.js');
var loading;

export default class NewsItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.title,
            writter: this.props.writter,
            thumb: this.props.thumb,
            created_at: this.props.created_at,
            text: '',

            loading: true
        }
    }
    async componentDidMount() {
        let response = await Api.request('news/' +
                this.props.id
            ,'GET'
        );
        loading.hide();
        if (response[0] == 200) {
            var news = response[1]['data']['news'];
            this.setState({title: news['title'], text: news['text'], thumb: news['thumb'], created_at: news['created_at'], writter: news['writter']});
        }
    }

    openUrl = (url) => {
        Linking
            .openURL(url)
            .catch(err => console.error('An error occurred',
                err
            ));
    }

    onNavigationStateChange(event) {
        if (event.title) {
            const htmlHeight = Number(event.title) //convert to number
            this.setState({Height:htmlHeight});
        }
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Header style={[Styles.HeaderWrapper]}>
                    <Button onPress={() => {
                            Actions.pop()
                        }} iconLeft="iconLeft" transparent="transparent">
                        <Icon name='chevron-left' style={[Styles.headerActionIcon]}/>
                        <View>
                            <Text style={[Styles.fontSans, Styles.headerActionText]}>بازگشت</Text>
                        </View>
                    </Button>
                    <Title style={[Styles.fontSans, Styles.headerTitleRight]}></Title>
                </Header>
                <Content theme={barghtheme} style={[Styles.mainWrapper]}>
                    <Loading default={true} ref={(ref) => loading = ref}/>
                    <ScrollView style={[Styles.container]}>
                        <View style={{
                                alignItems: 'flex-end',
                                paddingHorizontal: 20,
                                paddingBottom: 0
                            }}>
                            <Row style={{
                                    paddingBottom: 10
                                }}>
                                <Col style={{
                                        alignItems: 'flex-start'
                                    }}>
                                    <Image resizeMode={"cover"} source={{
                                            uri: this.state.thumb
                                        }} style={[
                                            Styles.newsCover, {
                                                width: 150,
                                                height: 112
                                            }
                                        ]}/>
                                </Col>
                                <Col>
                                    <Text style={[
                                            Styles.fontSans, {
                                                fontSize: 18
                                            }
                                            
                                        ]}>{this.state.title}</Text>
                                </Col>
                            </Row>
                            <Row>
                                <Col style={{
                                        alignItems: 'flex-start'
                                    }}>
                                    <Text style={[
                                            Styles.fontSansLight, {
                                                fontSize: 11
                                            }
                                        ]}>تاریخ ایجاد : {this.state.created_at}</Text>
                                </Col>
                                <Col>
                                    <Text style={[
                                            Styles.fontSansLight, {
                                                fontSize: 11
                                            }
                                        ]}>نویسنده : {this.state.writter}</Text>
                                </Col>
                            </Row>
                        </View>

                        <View style={[
                                Styles.innerContainer
                            ]}>
                              <WebView 
                                scrollEnabled={false}
                                javaScriptEnabled ={true}
                                scrollEnabled={false}
                                onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                                style={[{
                                flex: 1,
                                width: width - 30,
                                height:this.state.Height
                                    }
                                ]} source={{
                                    uri: config.server +
                                            '/api/news/show/news/' + this.props.id
                                }}/>
                        </View>
                    </ScrollView>
                </Content>

            </Container>
        );
    }
}
