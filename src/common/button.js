import React , { Component } from 'react';

import {
    View,
    StyleSheet,
    Text,
    TouchableHighlight,
} from 'react-native';

export default class Button extends Component {
    render (){
        return (
            <TouchableHighlight onPress={this.props.onPress}>
            <Text>{ this.props.text }</Text>
            </TouchableHighlight>
        );
    }
}
