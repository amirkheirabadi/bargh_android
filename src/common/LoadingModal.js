import React , { Component } from 'react';

import { View, Dimensions, Text, Modal, Image,
} from 'react-native';
var styles = require('../style.js');
import { Container, Content, InputGroup, Input, Icon, List, ListItem, Picker, Item, Button, Grid, Row } from 'native-base';
import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';

export default class LoadingModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: this.props.default
        };
    }

    show() {
        this.setState({
            show: ture
        });
    }

    hide() {
        this.setState({
            show: false
        });
    }

    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        return (
            <Modal animationType={ "fade" } transparent={ true } visible={ this.state.show }>
              <View style={ { backgroundColor: 'rgba(103, 85, 151, 0.95)', width: width, height: height, flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' } }>
                <View>
                  <View style={ { alignSelf: 'center', marginTop: 80 } }>
                    <Bars size={ 20 } color="#FF4C74" />
                  </View>
                  <View style={ [{ alignSelf: 'center', marginTop: 30 }] }>
                    <Text style={ [styles.fontSans, { color: '#FF4C74' }] }>در حال بارگذاری ...</Text>
                  </View>
                </View>
              </View>
            </Modal>
            );
    }
}
