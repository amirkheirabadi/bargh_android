import React, { Component } from 'react';
import { Container, Content, InputGroup, Input, Icon, Header, Title, Button, Grid, Row, Col, List, ListItem, Card, CardItem } from 'native-base';
import barghtheme from '../theme/barghchin';
import { Actions } from 'react-native-router-flux';
import { Image, Text, Modal, View, Dimensions, BackAndroid } from 'react-native';
import Api from '../helper/api';
import LoadingModal from '../common/LoadingModal';

var styles = require('../style.js');
var loading;
export default class Business extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: ''
    };
  }

  async componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress',
      () => {
        Actions.Home();
        return true;
      }
    );

    let user = await Api.userProfile();
    this.setState({
      user: user
    });
    loading.hide();
  }

  componentWillUnmount() {
    this
      .hardwareBackPress
      .remove();
  }

  render() {
    const width = Dimensions.get('window').width
    const height = Dimensions.get('window').height
    return (
      <Container>
        <Content theme={ barghtheme } style={ styles.mainWrapper }>
          <LoadingModal default={ true } ref={ (ref) => {
                                                 loading = ref
                                               } } />
          <Header style={ [{ flexDirection: 'row' }] }>
            <Button onPress={ () => {
                                Actions.Home()
                              } } iconLeft transparent>
              <Icon name='chevron-left' />
              <Text style={ [styles.fontSans, { fontSize: 14 }] }>بازگشت</Text>
            </Button>
            <Title style={ [styles.fontSans, { color: '#fff', textAlign: 'center', alignSelf: 'flex-end', fontSize: 14 }] }>مدیریت کسب و کارها</Title>
          </Header>
          <Text style={ [styles.fontSans, styles.wrapperHeader] }>لیست کسب و کارها</Text>
          <Card style={ [{ width: width * 0.97, alignSelf: 'center', }] }>
            <CardItem style={ [{ padding: 0 }] }>
              <List style={ [{ padding: 0 }] }>
                <ListItem style={ [{ padding: 0, height: 60 }] }>
                  <Col>
                  { this.state.user['company'] != undefined && typeof this.state.user['company'] ? (
                    <Row>
                      <Button danger iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                      this.checkDelete('company')
                                                                                    } }>
                        <Icon name='delete' />
                        <Text style={ [styles.fontSans, { fontSize: 10 }] }>حدف</Text>
                      </Button>
                      <Button info iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                    Actions.Company({
                                                                                      company: this.state.user['company']
                                                                                    })
                                                                                  } }>
                        <Icon name='edit' />
                        <Text style={ [styles.fontSans, { fontSize: 10 }] }>ویرایش</Text>
                      </Button>
                    </Row>
                    ) : (
                    <Button primary iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                     Actions.Company()
                                                                                   } }>
                      <Icon name='add' />
                      <Text style={ [styles.fontSans, { fontSize: 10 }] }>ایجاد</Text>
                    </Button>
                    ) }
                  </Col>
                  <Col>
                  <View>
                    <Text style={ [{ paddingRight: 15, paddingTop: 25, fontFamily: 'IRANSans_Medium' }] }>تولید کننده / فروشنده</Text>
                  </View>
                  </Col>
                </ListItem>
                <ListItem style={ [{ padding: 0, height: 60 }] }>
                  <Col>
                  { this.state.user['importer'] != undefined && typeof this.state.user['importer'] ? (
                    <Row>
                      <Button danger iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                      this.checkDelete('importer')
                                                                                    } }>
                        <Icon name='delete' />
                        <Text style={ [styles.fontSans, { fontSize: 10 }] }>حدف</Text>
                      </Button>
                      <Button info iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                    Actions.importer({
                                                                                      importer: this.state.user['importer']
                                                                                    })
                                                                                  } }>
                        <Icon name='edit' />
                        <Text style={ [styles.fontSans, { fontSize: 10 }] }>ویرایش</Text>
                      </Button>
                    </Row>
                    ) : (
                    <Button primary iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                     Actions.Importer()
                                                                                   } }>
                      <Icon name='add' />
                      <Text style={ [styles.fontSans, { fontSize: 10 }] }>ایجاد</Text>
                    </Button>
                    ) }
                  </Col>
                  <Col>
                  <View>
                    <Text style={ [{ paddingRight: 15, paddingTop: 25, fontFamily: 'IRANSans_Medium' }] }>وارد کننده</Text>
                  </View>
                  </Col>
                </ListItem>
                <ListItem style={ [{ padding: 0, height: 60 }] }>
                  <Col>
                  { this.state.user['producer'] != undefined && typeof this.state.user['producer'] ? (
                    <Row>
                      <Button danger iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                      this.checkDelete('producer')
                                                                                    } }>
                        <Icon name='delete' />
                        <Text style={ [styles.fontSans, { fontSize: 10 }] }>حدف</Text>
                      </Button>
                      <Button info iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                    Actions.producer({
                                                                                      producer: this.state.user['producer']
                                                                                    })
                                                                                  } }>
                        <Icon name='edit' />
                        <Text style={ [styles.fontSans, { fontSize: 10 }] }>ویرایش</Text>
                      </Button>
                    </Row>
                    ) : (
                    <Button primary iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                     Actions.Producer()
                                                                                   } }>
                      <Icon name='add' />
                      <Text style={ [styles.fontSans, { fontSize: 10 }] }>ایجاد</Text>
                    </Button>
                    ) }
                  </Col>
                  <Col>
                  <View>
                    <Text style={ [{ paddingRight: 15, paddingTop: 25, fontFamily: 'IRANSans_Medium' }] }>تولید کننده</Text>
                  </View>
                  </Col>
                </ListItem>
                <ListItem style={ [{ padding: 0, height: 60 }] }>
                  <Col>
                  { this.state.user['technicians'] != undefined && typeof this.state.user['زخپح'] ? (
                    <Row>
                      <Button danger iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                      this.checkDelete('technicians')
                                                                                    } }>
                        <Icon name='delete' />
                        <Text style={ [styles.fontSans, { fontSize: 10 }] }>حدف</Text>
                      </Button>
                      <Button info iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                    Actions.technicians({
                                                                                      technicians: this.state.user['technicians']
                                                                                    })
                                                                                  } }>
                        <Icon name='edit' />
                        <Text style={ [styles.fontSans, { fontSize: 10 }] }>ویرایش</Text>
                      </Button>
                    </Row>
                    ) : (
                    <Button primary iconRight style={ [{ height: 60 }] } onPress={ () => {
                                                                                     Actions.Technicians()
                                                                                   } }>
                      <Icon name='add' />
                      <Text style={ [styles.fontSans, { fontSize: 10 }] }>ایجاد</Text>
                    </Button>
                    ) }
                  </Col>
                  <Col>
                  <View>
                    <Text style={ [{ paddingRight: 15, paddingTop: 25, fontFamily: 'IRANSans_Medium' }] }>تکنسین</Text>
                  </View>
                  </Col>
                </ListItem>
              </List>
            </CardItem>
          </Card>
        </Content>
      </Container>
      );
  }
}
