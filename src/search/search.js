import React, {Component} from 'react';

import {
    Container,
    Content,
    InputGroup,
    Input,
    Header,
    Title,
    Button,
    Grid,
    Icon,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab
} from 'native-base';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import MapView from 'react-native-maps';
import Drawer from 'react-native-drawer';
import Filter from './filter';

import {
    Image,
    Text,
    View,
    Dimensions,
    Animated
} from 'react-native';
import Api from '../helper/api';

var styles = require('../style.js');
export default class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: {
                'latitude': '',
                'longitude': ''
            },
            fabs: {
                active: false
            },
            drawer : {
                drawerType: 'overlay',
                closedDrawerOffset: 0,
                panOpenMask: .1,
                panCloseMask: .5,
                relativeDrag: false,
                panThreshold: .25,
                tweenHandlerOn: false,
                tweenDuration: 200,
                tweenEasing: 'linear',
                disabled: false,
                tweenHandlerPreset: null,
                acceptDoubleTap: false,
                acceptTap: false,
                acceptPan: true,
                tapToClose: true,
                open:true,
                negotiatePan: false,
                active: 'true'
            },
            search:{
                type : '',
                distance : '',
                city_id: '',
                businessType : '',
                brand : '',
                category : '',
                search : '',
            }
        };
    }

    componentDidMount() {
       navigator.geolocation.getCurrentPosition(
         (position) => {
            this.setState({
                current : {
                    latitude : position.coords.latitude,
                    longitude: position.coords.longitude,
                }
            })
         },
         (error) => console.log(JSON.stringify(error)),
         {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000}
       );
    }

    gotToCurrent() {
        this.refs.map.animateToRegion({
            latitude: this.state.current.latitude,
            longitude: this.state.current.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },1000)
    }

    openDrawer() {
        this._drawer.open(false)
    }

    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height

        return (
            <Drawer
                ref={(ref) => this._drawer = ref}
                open={this.state.drawer.open}
                type={this.state.drawer.drawerType}
                animation={this.state.drawer.animation}
                openDrawerOffset={(viewport) => (viewport.width / 2) - 30}
                closedDrawerOffset={this.state.drawer.closedDrawerOffset}
                panOpenMask={this.state.drawer.panOpenMask}
                panCloseMask={this.state.drawer.panCloseMask}
                relativeDrag={this.state.drawer.relativeDrag}
                panThreshold={this.state.drawer.panThreshold}
                disabled={this.state.drawer.disabled}
                tweenHandler={(ratio) => ({
                main: {
                    opacity: (2 - ratio) / 2
                }
                })}
                content={<Filter search={this.state.search}/>}
                tweenDuration={this.state.drawer.tweenDuration}
                tweenEasing={this.state.drawer.tweenEasing}
                acceptDoubleTap={this.state.drawer.acceptDoubleTap}
                acceptTap={this.state.drawer.acceptTap}
                acceptPan={this.state.drawer.acceptPan}
                tapToClose={this.state.drawer.tapToClose}
                negotiatePan={this.state.drawer.negotiatePan}
                changeVal={this.state.drawer.changeVal}
                side='right'>
                    <Container>
                        <Content theme={barghtheme}>
                            <Header style={[{
                                    flexDirection: 'row'
                                }
                            ]}>
                                <Button onPress={() => {
                                    Actions.pop()
                                }} iconLeft transparent>
                                    <Icon name='chevron-left'/>
                                    <Text>بازگشت</Text>
                                </Button>
                                <Title></Title>

                                <Button onPress={() => {this.openDrawer()}} iconRight transparent>
                                    <Icon name='filter-list'/>
                                    <Text>فیلترها</Text>
                                </Button>
                            </Header>
                            <MapView
                                ref="map"
                                style={[{width:width,height:height}]}
                                initialRegion = {{
                                    latitude: 35.696111,
                                    longitude: 51.423055999999974,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}
                             >
                         </MapView>
                    </Content>
                </Container>
                <Fab
                   active={this.state.fabs.active}
                   direction="right"
                   containerStyle={{ marginLeft: 10 }}
                   style={{ backgroundColor: '#5c4b8c' }}
                   position="bottomLeft"
                   onPress={() => this.gotToCurrent()}
                   >
                   <Icon name="my-location" />
               </Fab>
            </Drawer>
        );
    }
}
