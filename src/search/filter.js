import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    CheckBox,
    Radio,
    Picker,
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import {Actions} from 'react-native-router-flux';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import {Text, View, Dimensions, TouchableWithoutFeedback, Modal} from 'react-native';
import Api from '../helper/api';
var styles = require('../style.js');

export default class Filter extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            locationType: 'city',
            distance: '',
            user: '',
            business: [],
            provinces: [],
            cities: []
        };
    }

    async componentDidMount() {
        let provinces = await Api.request('provinces', 'POST');
        if (provinces[0] == 200) {
            this.setState({
                provinces: provinces[1]['data']
            });
        }
    }

    async chooseProvince(value) {
        console.log(value);
        let cities = await Api.request('cities', 'POST', {
            'province' : value
        });
        if (cities[0] == 200) {
            this.setState({
                cities: cities[1]['data']
            });
        }
    }

    changeType(){
        if (this.state.locationType == "city") {
            this.setState({
                locationType : 'distance'
            });
        } else {
            this.setState({
                locationType : 'city'
            });
        }
    }

    changeBusiness(business) {
        var data = this.state.business;
        if (data.indexOf(business) > -1) {
            index = data.indexOf(business);
            data.splice(index, 1);
        } else {
            data.push(business);
        }
        this.setState({
            business: data
        });
    }

    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        const Item = Picker.Item;
        return (

            <Container>
                <Content theme={barghtheme} style={[
                    styles.mainWrapper, {
                        backgroundColor: '#5c4b8c'
                    }
                ]}>
                    <Row style={[{marginBottom: 0}]}>
                        <Col>
                            <Button block style={[{marginTop:15,marginLeft:5,backgroundColor:'#3498db'}]} onPress={() => {
                                this.setState({
                                    isCollapsed: !this.state.isCollapsed
                                })

                            }}>
                                اعمال فیلترها
                            </Button>
                        </Col>
                        <Col>
                            <Text style={[
                                styles.fontSans,
                                styles.wrapperHeader, {
                                    marginTop: 15,
                                    color: '#fff'
                                }
                            ]}>جستجو</Text>
                        </Col>
                    </Row>


                    <Card style={[{
                            width: width * 0.53,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0
                            }
                        ]}>
                        <InputGroup>
                            <Icon name='search' style={{color:'#384850'}}/>
                            <Input inlineLabel placeholder="نام کسب و کار" value={this.state.search} onChangeText={(text) => {
                                this.setState({search: text});
                            }}/>
                        </InputGroup>
                        </CardItem>
                    </Card>

                    <Text style={[
                        styles.fontSans,
                        styles.wrapperHeader, {
                            marginTop: 0,
                            color: '#fff'
                        }
                    ]}>منطقه جغرافیایی</Text>
                    <Card style={[{
                            width: width * 0.53,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 5
                            }
                        ]}>
                        <Grid>
                        <Row style={[{alignSelf:'flex-end'}]}>
                            <Text style={[styles.fontSans]}>جستجو بر حسب موقعیت فعلی</Text>
                            <Radio selected={this.state.locationType != "city"} onPress={() => {this.changeType()}} style={[{marginLeft: 10}]}/>
                        </Row>
                        <Row>
                            <Picker  style={[{
                                width: width * 0.50,
                            }]}
                            mode="dropdown"
                            enabled={this.state.locationType == "distance"}
                            selectedValue={this.state.distance}
                            onValueChange={(value) => {
                                this.setState({
                                  distance : value
                                });
                            }}>
                                <Item label="۱ کیلومتر" value="1"/>
                                <Item label="۲ کیلومتر" value="2"/>
                                <Item label="۵ کیلومتر" value="5"/>
                                <Item label="۱۰ کیلومتر" value="10"/>
                            </Picker>
                        </Row>
                        <Row style={[{alignSelf:'flex-end',marginTop:10}]}>
                            <Text style={[styles.fontSans]}>جستجو بر حسب شهر</Text>
                            <Radio selected={this.state.locationType == "city"}  onPress={() => {this.changeType()}} style={[{marginLeft: 10}]}/>
                        </Row>
                        <Row style={[{alignSelf:'flex-end',marginTop:10}]}>
                            <Picker mode="dropdown"
                            enabled={this.state.locationType == "city"}
                            onValueChange={this.chooseProvince.bind(this)}
                            style={[{
                                width: width * 0.50,
                            }]}>
                            { this.state.provinces.map((s, i) => {
                                return <item
                                 value={s.id}
                                 label={s.name} />
                             }) }
                            </Picker>
                        </Row>

                        <Row style={[{alignSelf:'flex-end'}]}>
                            <Picker mode="dropdown"
                            enabled={this.state.locationType == "city"}
                            onValueChange={(value) => {
                                this.setState({
                                  search : {
                                      city_id : value
                                  }});
                            }}
                            style={[{
                                width: width * 0.50,
                            }]}>
                            { this.state.cities.map((s, i) => {
                                return <item
                                 value={s.id}
                                 label={s.name} />
                             }) }
                            </Picker>
                        </Row>
                        </Grid>
                        </CardItem>
                    </Card>
                    <Card style={[{
                            width: width * 0.53,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 5
                            }
                        ]}>
                            <Grid>
                                <Row  style={[{alignSelf:'flex-end'}]}>
                                    <Text style={[styles.fontSans,{marginRight: 10}]}>شرکت ها و فروشندگان</Text>
                                    <CheckBox checked={this.state.business.indexOf('company') > -1} onPress={() => {this.changeBusiness('company')}}/>
                                </Row>
                                <Row  style={[{alignSelf:'flex-end', marginTop:15}]}>
                                    <Text style={[styles.fontSans,{marginRight: 10}]}>تولید کنندگان</Text>
                                    <CheckBox checked={this.state.business.indexOf('producer') > -1} onPress={() => {this.changeBusiness('producer')}}/>
                                </Row>
                                <Row  style={[{alignSelf:'flex-end', marginTop:15}]}>
                                    <Text style={[styles.fontSans,{marginRight: 10}]}>وارد کنندگان</Text>
                                    <CheckBox checked={this.state.business.indexOf('importer') > -1} onPress={() => {this.changeBusiness('importer')}}/>
                                </Row>
                                <Row  style={[{alignSelf:'flex-end', marginTop:15}]}>
                                    <Text style={[styles.fontSans,{marginRight: 10}]}>تکنسین ها</Text>
                                    <CheckBox checked={this.state.business.indexOf('technician') > -1} onPress={() => {this.changeBusiness('technician')}}/>
                                </Row>
                            </Grid>
                        </CardItem>
                    </Card>

                    <TouchableWithoutFeedback onPress={() => {
                        this.setState({
                            type: 'name'
                        })
                    }}>
                        <Text style={[
                        styles.fontSans,
                        styles.wrapperHeader, {
                            marginTop: 0,
                            color: '#fff'
                        }
                    ]}>جستجو بر حسب محصول</Text>
                    </TouchableWithoutFeedback>
                    <Card style={[{
                            width: width * 0.53,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0
                            }
                        ]}>
                        <TouchableWithoutFeedback onPress={() => {
                            Actions.Products();
                        }}>
                            <View style={[{flexDirection:'row',alignSelf: 'flex-end',padding:5}]}>
                                <Text style={[styles.fontSans,{paddingTop:3}]}>انتخاب محصول</Text>
                                <Icon name="inbox" style={{
                                    color: '#bdc3c7',
                                    paddingLeft: 10
                                }}/>
                            </View>
                        </TouchableWithoutFeedback>
                        </CardItem>
                    </Card>                        

                    <Text style={[
                        styles.fontSans,
                        styles.wrapperHeader, {
                            marginTop: 0,
                            color: '#fff'
                        }
                    ]}>جستجو بر حسب برند</Text>
                    <Card style={[{
                            width: width * 0.53,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0,
                            }
                        ]}>
                            <TouchableWithoutFeedback onPress={() => {
                                Actions.Brands({
                                    new: "no"
                                });
                            }}>
                                <View style={[{flexDirection:'row',alignSelf: 'flex-end',padding:5}]}>
                                    <Text style={[styles.fontSans,{paddingTop:3}]}>انتخاب برند</Text>
                                    <Icon name="extension" style={{
                                        color: '#bdc3c7',
                                        paddingLeft: 10
                                    }}/>
                                </View>
                            </TouchableWithoutFeedback>
                        </CardItem>
                    </Card>


                    <Text style={[
                        styles.fontSans,
                        styles.wrapperHeader, {
                            marginTop: 0,
                            color: '#fff'
                        }
                    ]}>جستجو بر حسب تخصص</Text>
                    <Card style={[{
                            width: width * 0.53,
                            alignSelf: 'center'
                        }
                    ]}>
                        <CardItem style={[{
                                padding: 0,
                            }
                        ]}>
                            <TouchableWithoutFeedback onPress={() => {
                                Actions.Skill({
                                    new: "no"
                                });
                            }}>
                                <View style={[{flexDirection:'row',alignSelf: 'flex-end',padding:5}]}>
                                    <Text style={[styles.fontSans,{paddingTop:3}]}>انتخاب تخصص</Text>
                                    <Icon name="gavel" style={{
                                        color: '#bdc3c7',
                                        paddingLeft: 10
                                    }}/>
                                </View>
                            </TouchableWithoutFeedback>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}
