'use strict';

import React from 'react';
import {
    Text,
    View,
    Image,
    Navigator,
    TouchableWithoutFeedback,
    Modal
} from 'react-native';
import Styles from './style';
import {Bubbles, DoubleBounce, Bars, Pulse} from 'react-native-loader';

export default class Loading extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            themeColor: '#fff'
        }
    }

    async componentDidMount() {
        if (this.props.default) {
            this.setState({show: true});
        }
    }

    hide = () => {
        this.setState({show: false});
    }

    show = () => {
        this.setState({show: false});
    }

    render() {
        return (
            <Modal animationType={"fade"} transparent={true} visible={this.state.show} onRequestClose={() => this.setState({show: false})}>
                <View style={[Styles.loadingWrapper]}>
                    <View style={Styles.loadingLayout}>
                     <View style={[Styles.loadingSpinner]}>
                            <Bubbles size={10} color="#fff"/>
                        </View>
                        <Text style={[Styles.fontSans, Styles.loadingText]}>لطفا منتظر بمانید ...</Text>
                    </View>
                </View>
            </Modal>
        )
    }
}