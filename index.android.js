/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import {
	AppRegistry,
	NetInfo
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Main from './src/main';

function handleFirstConnectivityChange(isConnected) {
	if(!isConnected) {
		Actions.Offline();
	}
}

NetInfo.isConnected.fetch().then(isConnected => {
	if(!isConnected) {
		Actions.Offline();
	}
});

NetInfo.isConnected.addEventListener(
	'change',
	handleFirstConnectivityChange
);
AppRegistry.registerComponent('bargh_android', () => Main);
